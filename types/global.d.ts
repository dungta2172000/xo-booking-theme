// https://stackoverflow.com/questions/71517082/how-to-share-a-global-d-ts-from-several-packages-in-monorepo

declare const xoUpdate: number;
declare const xoDfLang: string;
declare const xoMoneyFormat: string;
declare const Shopify: {
  shop: string;
  locale: string;
  designMode: boolean;
};

declare const xoCustomerId: string;
declare const xoExpireDay: number;
declare interface ImportMeta {
  env: {
    VITE_NODE_ENV: string;
    VITE_CDN_URL: string;
    VITE_APP_ENDPOINT_PROD: string;
  };
}
declare module '*.scss';
