module.exports = {
  env: {
    "browser": true,
    "es2021": true
  },
  extends: [
    "airbnb-base",
    "airbnb-typescript/base",
    "plugin:import/recommended",
    "plugin:import/typescript",
    "preact",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12,
    sourceType: "module"
  },
  ignorePatterns: ["vite.config.js", "tsconfig.json", ".eslintrc.js", ".eslintrc.cjs"],
  rules: {
    "no-param-reassign": "off",
    "import/prefer-default-export": "off",
    "max-len": ["error", {
      "code": 180,
      "ignorePattern": "\/\*\* \@xstate-layout .*",
    }]
  },
}
