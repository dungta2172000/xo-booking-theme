import { FunctionComponent } from 'preact';
import clsx from 'clsx';
import { Error } from '..';
import './checkbox.scss';

type TProps = {
  text: string;
  id?: string;
  value: string;
  disabled?: boolean;
  labelHidden?: boolean;
  helpText?: string;
  checked?: boolean;
  className?: string;
  required?: boolean;
  isPartialCheck?: boolean;
  error?: string;
  onBlur?: (e: any) => void;
  onChange: (value: boolean) => void;
};

const Checkbox: FunctionComponent<TProps> = ({
  text,
  id,
  value,
  labelHidden,
  helpText,
  checked,
  className,
  disabled,
  onBlur,
  onChange,
  isPartialCheck,
  error,
}) => {
  const handleOnChange = (val: boolean) => {
    if (!disabled) {
      onChange(val);
    }
  };

  const handleBlur = (e: any) => {
    if (onBlur) {
      onBlur(e);
    }
  };

  return (
    <div
      className={clsx(
        'xo-form-field__main',
        'xo-form-checkbox',
        disabled && 'xo-form__field--disabled',
        className,
      )}
      id={`xo-form-field__main--${id}`}
    >
      <label className="xo-form-checkbox__label" htmlFor={`xo-form__field--${id}`}>
        <span className="xo-form-checkbox__control">
          <span
            className={clsx(
              'xo-form-checkbox__check',
              (checked || isPartialCheck) && 'xo-form-checkbox__check--checked',
            )}
          >
            <input
              id={`xo-form__field--${id}`}
              type="checkbox"
              className="xo-form-checkbox__input"
              value={value}
              checked={checked}
              onChange={(e) => {
                handleOnChange((e.target as HTMLInputElement).checked);
                (e.target as HTMLInputElement).focus();
              }}
              onBlur={handleBlur}
            />
            <span className="xo-form-checkbox__backdrop" />
            <span className="xo-form-checkbox__icon">
              <span className="xo-form-checkbox__status">
                {checked && (
                  <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                    {/* eslint-disable-next-line max-len */}
                    <path d="M14.723 6.237a.94.94 0 0 1 .053 1.277l-5.366 6.193a.834.834 0 0 1-.611.293.83.83 0 0 1-.622-.264l-2.927-3.097a.94.94 0 0 1 0-1.278.82.82 0 0 1 1.207 0l2.297 2.43 4.763-5.498a.821.821 0 0 1 1.206-.056Z" />
                  </svg>
                )}
                {!checked && isPartialCheck && <span className="xo-form-checkbox__status-blank" />}
              </span>
            </span>
          </span>
        </span>
        {!labelHidden && (
          <span
            className="xo-form-checkbox__label-text"
            dangerouslySetInnerHTML={{ __html: text }}
          />
        )}
      </label>
      {helpText && (
        <div className="xo-form-checkbox__help-text">
          <span>{helpText}</span>
        </div>
      )}
      {error && <Error error={error} />}
    </div>
  );
};

export default Checkbox;
