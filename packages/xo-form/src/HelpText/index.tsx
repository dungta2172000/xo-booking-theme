import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import './label.scss';

type TProps = {
  text?: string;
  position?: 'above' | 'below';
};

const HelpText: FunctionComponent<TProps> = ({ text, position }) => (
  <div className={clsx('xo-form-help', position && `xo-form-help--${position}`)}>{text}</div>
);

export default HelpText;
