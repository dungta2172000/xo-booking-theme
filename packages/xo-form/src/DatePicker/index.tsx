import clsx from 'clsx';
import { Error, HelpText, Label } from '..';
import { IFieldCommon } from '../types/field';
import './datepicker.scss';

interface IDatePicker extends IFieldCommon {
  onChange: (value: any) => void;
  hideField?: boolean;
  errorMessage: string | undefined;
  hasError?: boolean;
}

const DatePicker = (props: IDatePicker) => {
  const {
    required,
    label,
    value,
    labelHidden,
    id,
    helpText,
    onChange,
    errorMessage,
    name,
    onBlur,
  } = props;
  const handleChange = (e: any) => {
    const { value: val1 } = e.target;
    onChange(val1);
  };

  return (
    <div className="xo-form-datepicker">
      {!labelHidden && <Label label={label} id={id} required={required} />}
      <input
        className={clsx(
          'xo-form-datepicker__input',
          errorMessage && 'xo-form-datepicker__input--err',
        )}
        id={id}
        type="date"
        name={name}
        onChange={handleChange}
        value={value}
        onBlur={onBlur}
      />
      {helpText && <HelpText text={helpText} position="below" />}
      {errorMessage && <Error error={errorMessage} />}
    </div>
  );
};

export default DatePicker;
