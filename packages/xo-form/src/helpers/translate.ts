import { TMultiLanguage } from '../types/field';

export const t = (text?: TMultiLanguage | string, locale: string = 'en'): string => {
  if (typeof text === 'string') {
    return text;
  }

  if (!text || typeof text === 'undefined' || !Object.keys(text).length) {
    return '';
  }

  if (typeof text === 'string') {
    return text;
  }

  if (text[locale] != null) {
    return text[locale];
  }

  // if (xoDfLang && text[xoDfLang]) {
  //   return text[xoDfLang];
  // }

  return text[Object.keys(text)[0]];
};
