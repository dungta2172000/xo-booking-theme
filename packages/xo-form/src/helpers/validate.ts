const isEmptyValue = (type: string, value: string | number): boolean => {
  if (type === 'number') {
    if (value === 0) {
      return true;
    }
  } else {
    // eslint-disable-next-line no-lonely-if
    if ((value as string).trim() === '') {
      return true;
    }
  }
  return false;
};

const isEmailValid = (value: string): boolean => {
  const test = !/^([\w\d._\-#])+@([\w\d._\-#]+[.][\w\d._\-#]+)+$/.test(value as string);
  return test;
};

export { isEmptyValue, isEmailValid };
