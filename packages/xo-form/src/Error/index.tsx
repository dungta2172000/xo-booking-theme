import { FunctionComponent } from 'preact';
import './error.scss';

type TProps = {
  error: string,
};

const Error: FunctionComponent<TProps> = ({ error }) => (
  <div className='xo-form-error'>
    <div className='xo-form-error__container'>
      <div className='xo-form-error__icon'>
        <span className='xo-form-error__icon-content'>
          <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
            <path d="M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-9a1 1 0 0 0 2 0v-2a1 1 0 1 0-2 0v2zm0 4a1 1 0 1 0 2 0 1 1 0 0 0-2 0z" />
          </svg>
        </span>
      </div>
      {error}
    </div>
  </div>
);

export default Error;
