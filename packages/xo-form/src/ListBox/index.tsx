import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';
import { Error, Label } from '..';
import { TChoice } from '../types/field';
import './listBox.scss';

type TProps = {
  label?: string;
  hideLabel?: boolean;
  selected: string;
  options: TChoice[];
  required?: boolean;
  error?: string;
  onSelect: (value: string) => void;
  onBlur?: (e: any) => void;
};

const ListBox: FunctionComponent<TProps> = ({
  label,
  hideLabel,
  selected,
  options,
  required,
  error,
  onSelect,
  onBlur,
}) => {
  const [errorMessage, setErrowMessage] = useState<string>(error || '');

  const handleSelect = (val: string) => {
    onSelect(val);
    if (required) {
      setErrowMessage('');
    }
  };

  const handleBlur = (e: any) => {
    if (onBlur) {
      onBlur(e);
    }
    if (required && selected === '') {
      setErrowMessage(`${label || 'This field'} is required`);
    }
  };

  return (
    <>
      {label && !hideLabel && <Label label={label} />}
      <div className="xo-form-list-box">
        <ul className="xo-form-list-box__list" onBlur={handleBlur}>
          {options.map((option) => (
            <li
              key={option.value}
              className={clsx(
                'xo-form-list-box__option',
                selected === option.value && 'xo-form-list-box__option--selected',
              )}
              onClick={() => handleSelect(option.value)}
            >
              <div className="xo-form-list-box__text">
                <div className="xo-form-list-box__option--content">{option.label}</div>
              </div>
            </li>
          ))}
        </ul>
        {errorMessage && <Error error={errorMessage} />}
      </div>
    </>
  );
};

export default ListBox;
