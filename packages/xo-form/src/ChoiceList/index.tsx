/* eslint-disable linebreak-style */
import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { Checkbox, Error, HelpText, Label, RadioButton, TextField } from '..';
import { t } from '../helpers/translate';
import { OtherValue, TChoice } from '../types/field';
import './choiceList.scss';

type TProps = {
  id?: string;
  title?: string;
  choices: TChoice[];
  titleHidden?: boolean;
  selected: string[] | string;
  name: string;
  allowMultiple?: boolean;
  className?: string;
  helpText?: string;
  required?: boolean;
  disabled?: boolean;
  otherValue?: OtherValue;
  error?: string;
  onSelect: (value: string[] | string) => void;
  onBlur?: (e: any) => void;
};

const ChoiceList: FunctionComponent<TProps> = ({
  id,
  title,
  choices,
  titleHidden,
  selected,
  className,
  helpText,
  disabled,
  name,
  otherValue,
  allowMultiple,
  error,
  onSelect,
  onBlur,
}) => {
  const [otherText, setOtherText] = useState<string>('');
  const [selectOther, setSelectOther] = useState<boolean>(false);

  useEffect(() => {
    if (!selected.length) {
      setSelectOther(false);
      setOtherText('');
    }
  }, [selected]);

  const haveCheckedMultiple = (val: string) => selected.includes(val);
  const haveCheckedSingle = (val: string) => selected === val;

  const handleChange = (value: string) => {
    if (value === `${id}-other`) {
      if (selectOther) {
        onSelect((selected as string[]).filter((select) => select !== otherText));
      } else {
        if (allowMultiple) {
          onSelect([...selected, otherText]);
        } else {
          onSelect([otherText]);
        }
      }

      setSelectOther(!selectOther);
    } else {
      if (!allowMultiple) setSelectOther(false);
      // add value trong option lên đầu, other value ở sau cùng
      if (allowMultiple) {
        if (selected.includes(value)) {
          onSelect((selected as string[]).filter((item) => item !== value));
        } else {
          onSelect([value, ...selected]);
        }
      } else {
        onSelect(value);
      }
    }
  };

  const handleBlur = (e: any) => {
    if (onBlur) {
      onBlur(e);
    }
  };

  const handleOtherChange = (value: string) => {
    if (allowMultiple) {
      const selectedVar = [...selected];
      selectedVar[selectedVar.length - 1] = value;
      onSelect(selectedVar);
    } else {
      onSelect([value]);
    }
    setOtherText(value);
  };

  return (
    <fieldset
      className={clsx(
        'xo-form-choice-list',
        'xo-form-field__main',
        disabled && 'xo-form__field--disabled',
        className,
      )}
      id={`xo-form-field__main--${id}`}
    >
      {title && !titleHidden && <Label label={title} id={id} disabled={disabled || false} />}
      <ul className="xo-form-choice-list__choices">
        {choices.map((choice) => (
          <li key={choice.value}>
            {allowMultiple ? (
              <Checkbox
                text={t(choice.label)}
                value={choice.value}
                id={choice.id}
                disabled={choice.disabled}
                checked={haveCheckedMultiple(choice.value)}
                onChange={() => handleChange(choice.value)}
                onBlur={handleBlur}
              />
            ) : (
              <RadioButton
                id={choice.id}
                name={name || `name-field--${id}`}
                label={t(choice.label)}
                value={choice.value}
                disabled={choice.disabled}
                checked={haveCheckedSingle(choice.value)}
                onChange={() => handleChange(choice.value)}
                onBlur={handleBlur}
              />
            )}
          </li>
        ))}
        {otherValue?.allowOtherValue && (
          <li>
            {allowMultiple ? (
              <Checkbox
                id={`${id}-other`}
                text={t(otherValue!.value)}
                value={otherText}
                checked={selectOther}
                onChange={() => handleChange(`${id}-other`)}
                onBlur={handleBlur}
              />
            ) : (
              <RadioButton
                id={`${id}-other`}
                name={name || `name-field--${id}`}
                label={t(otherValue!.value)}
                value={otherText}
                checked={selectOther}
                onChange={() => handleChange(`${id}-other`)}
                onBlur={handleBlur}
              />
            )}
          </li>
        )}
      </ul>
      {selectOther && (
        <div className="xo-form-select--other">
          <TextField
            label="Other value"
            type="text"
            value={otherText}
            onChange={handleOtherChange}
          />
        </div>
      )}
      {error && <Error error={error} />}
      {helpText && <HelpText text={helpText} position="below" />}
    </fieldset>
  );
};

export default ChoiceList;
