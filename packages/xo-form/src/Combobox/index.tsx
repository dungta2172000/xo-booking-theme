import clsx from 'clsx';
import { FunctionComponent, VNode } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import { HelpText, Label, ListBox, ChoiceList, Error } from '..';
import { t } from '../helpers/translate';
import { TChoice } from '../types/field';
import './combobox.scss';

type TProps = {
  id?: string;
  label?: string;
  hideLabel?: boolean;
  allowMultiple?: boolean;
  open: boolean;
  value: string | string[];
  className?: string;
  search: string;
  activator: VNode;
  options: TChoice[];
  helpText?: string;
  required?: boolean;
  disabled?: boolean;
  error?: string;
  onClose: () => void;
  onSelect: (val: string | string[]) => void;
  onBlur?: (e: any) => void;
};

const Combobox: FunctionComponent<TProps> = ({
  id,
  label,
  hideLabel,
  allowMultiple,
  open,
  onClose,
  activator,
  helpText,
  className,
  search,
  options,
  required,
  disabled,
  error,
  onSelect,
  onBlur,
  value,
}) => {
  const mainRef = useRef<HTMLDivElement>(null);
  const comboxRef = useRef<HTMLDivElement>(null);
  const [errorMessage, setErrowMessage] = useState<string>(error || '');
  const [openCom, setOpenCom] = useState<boolean>(open);
  const [orient, setOrient] = useState<'top' | 'bottom'>();
  const [isText, setIsText] = useState<boolean>(false);

  const handleClose = () => {
    document
      .querySelector('.xo-form-combo-box__content')
      ?.classList.add('xo-form-combo-box__content--hidden');
    setTimeout(() => {
      onClose();
      setOpenCom(false);
    }, 300);
  };

  const handleBlur = (e: any) => {
    if (onBlur) {
      onBlur(e);
    }
    if (required) {
      if (allowMultiple) {
        if (!(value as string[]).length) {
          setErrowMessage(`${label || 'This field'} is required`);
        }
      } else {
        // eslint-disable-next-line no-lonely-if
        if ((value as string) === '') {
          setErrowMessage(`${label || 'This field'} is required`);
        }
      }
    }
  };

  const handleSelect = (val: string | string[]) => {
    if (allowMultiple) {
      onSelect(val);
    } else {
      onSelect(val);
      if (options.filter((item) => item.value === val)[0].label === search) {
        setIsText(true);
      } else {
        setIsText(false);
      }
      onClose();
    }
  };

  const removeExistingItem = (val: string) => {
    const newValue = (value as string[]).filter((item) => item !== val);
    handleSelect(newValue);
  };

  useEffect(() => {
    if (open) {
      setOpenCom(true);
    } else {
      handleClose();
    }
  }, [open]);

  useEffect(() => {
    if (!options.filter((item) => item.label === search).length) {
      setIsText(true);
    }
  }, [search]);

  useEffect(() => {
    const closeOpenModal = (e: MouseEvent) => {
      if (
        mainRef.current &&
        !mainRef.current.contains(e.target as Node) &&
        comboxRef.current &&
        !comboxRef.current.contains(e.target as Node)
      ) {
        onClose();
      }
    };

    document.addEventListener('mousedown', closeOpenModal);
    return () => document.removeEventListener('mousedown', closeOpenModal);
  }, []);

  useEffect(() => {
    const handleOrient = () => {
      const childHeight = comboxRef.current?.offsetHeight!;
      // eslint-disable-next-line no-unsafe-optional-chaining
      const distanceFromBottom =
        window.innerHeight -
        mainRef.current?.getBoundingClientRect().top! -
        mainRef.current?.offsetHeight!;
      const oriented = distanceFromBottom > 12 + childHeight ? 'bottom' : 'top';

      setOrient(oriented);
    };
    handleOrient();
    document.addEventListener('scroll', handleOrient);
    return () => document.removeEventListener('scroll', handleOrient);
  }, [openCom, comboxRef.current?.offsetHeight]);

  return (
    <>
      <div
        ref={mainRef}
        className={clsx('xo-form-field__main', disabled && 'xo-form__field--disabled', className)}
        id={`xo-form-field__main--${id}`}
      >
        {label && !hideLabel && <Label id={id} label={t(label)} disabled={disabled} />}
        <div className="xo-form-combo-box">
          <div>{activator}</div>
          {allowMultiple && value.length !== 0 && (
            <div className="xo-form-combo-box__stack">
              {(value as string[]).map((item) => (
                <div className="xo-form-combo-box__item" key={item}>
                  <span>{item}</span>
                  <button onClick={() => removeExistingItem(item)}>
                    <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                      {/* eslint-disable-next-line max-len */}
                      <path d="M6.707 5.293a1 1 0 0 0-1.414 1.414l3.293 3.293-3.293 3.293a1 1 0 1 0 1.414 1.414l3.293-3.293 3.293 3.293a1 1 0 0 0 1.414-1.414l-3.293-3.293 3.293-3.293a1 1 0 0 0-1.414-1.414l-3.293 3.293-3.293-3.293Z" />
                    </svg>
                  </button>
                </div>
              ))}
            </div>
          )}
          <div
            id="combobox_lewlew"
            className="xo-form-combo-box__combobox"
            style={
              orient === 'top' ? { top: `-${comboxRef.current?.offsetHeight}px` } : { top: '52px' }
            }
            ref={comboxRef}
          >
            {openCom && (
              <div className="xo-form-combo-box__content">
                {allowMultiple ? (
                  <ChoiceList
                    title={label}
                    titleHidden={hideLabel}
                    choices={options.filter((option) =>
                      t(option.label).toLowerCase().includes(search.toLowerCase()),
                    )}
                    selected={value as string[]}
                    name="lew lew"
                    allowMultiple
                    onSelect={(val) => handleSelect(val)}
                    onBlur={handleBlur}
                  />
                ) : (
                  <ListBox
                    label={label}
                    hideLabel={hideLabel}
                    onSelect={(val: any) => handleSelect(val)}
                    selected={value as string}
                    options={
                      isText
                        ? options.filter((option) =>
                            t(option.label).toLowerCase().includes(search.toLowerCase()),
                          )
                        : options
                    }
                    onBlur={handleBlur}
                  />
                )}
              </div>
            )}
          </div>
        </div>
        {errorMessage && <Error error={errorMessage} />}
        {helpText && <HelpText text={helpText} position="below" />}
      </div>
    </>
  );
};

export default Combobox;
