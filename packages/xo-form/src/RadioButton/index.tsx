import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import './radiobutton.scss';

type TProps = {
  label: string;
  helpText?: string;
  value: string;
  checked?: boolean;
  disabled?: boolean;
  id?: string;
  name: string;
  onBlur?: (e: any) => void;
  onChange: (newValue: string) => void;
};

const RadioButton: FunctionComponent<TProps> = ({
  label,
  value,
  id,
  name,
  checked,
  disabled,
  onBlur,
  onChange,
}) => {
  const handleChange = (val: string) => {
    if (!disabled) {
      onChange(val);
    }
  };

  return (
    <div>
      <label
        className={clsx('xo-form-radio-button', disabled && 'xo-form-radio-button--disabled')}
        for={`xo-form__field--${id}`}
      >
        <span className="xo-form-radio-button__control">
          <input
            name={name}
            className="xo-form-radio-button__radio-input"
            type="radio"
            id={`xo-form__field--${id}`}
            value={value}
            checked={checked}
            disabled={disabled}
            onBlur={onBlur}
            onChange={() => handleChange(value)}
          />
          <span className="xo-form-radio-button__backdrop" />
        </span>
        <span className="xo-form-radio-button__label-text">{label}</span>
      </label>
    </div>
  );
};

export default RadioButton;
