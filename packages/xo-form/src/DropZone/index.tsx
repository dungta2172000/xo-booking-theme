import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import { useRef } from 'preact/hooks';
import { HelpText, Label, Error } from '..';
import { IFieldCommon } from '../types/field';
import './dropzone.scss';

interface IProps extends IFieldCommon {
  files: File[];
  accept?: string;
  helpText?: string;
  type?: 'file' | 'image' | 'video';
  error?: string;
  hint?: string;
  className?: string;
  multiple?: boolean;
  disabled?: boolean;
  onBlur?: (e: any) => void;
  onDrop: (file: File[]) => void;
}

const DropZone: FunctionComponent<IProps> = ({
  label,
  labelHidden,
  helpText,
  required,
  disabled,
  className,
  files,
  id,
  accept,
  type,
  error,
  hint,
  multiple,
  onDrop,
  onBlur,
}) => {
  const inputFileRef = useRef<HTMLInputElement>(null);

  const handleChange = (fileList: FileList) => {
    onDrop(Array.from(fileList));
  };

  const handleOpenSelectFile = () => {
    inputFileRef.current?.click();
  };

  const handleDrop = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
    onDrop(Array.from(e.dataTransfer?.files!));
  };

  const handleDragLeave = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const handleDragOver = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const handleDragEnter = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };

  return (
    <div
      className={clsx('xo-form-field__main', disabled && 'xo-form__field--disabled', className)}
      id={`xo-form-field__main--${id}`}
    >
      {label && !labelHidden && (
        <Label label={label} id={id} required={required} disabled={disabled || false} />
      )}
      <div
        className="xo-form-drop-zone"
        onClick={handleOpenSelectFile}
        onDragEnter={handleDragEnter}
        onDragLeave={handleDragLeave}
        onDragOver={handleDragOver}
        onDrop={handleDrop}
        onBlur={onBlur}
        tabIndex={-1}
      >
        <span className="xo-form-drop-zone__visual">
          <input
            ref={inputFileRef}
            type="file"
            multiple={multiple}
            accept={accept}
            onChange={(e) => handleChange((e.target as HTMLInputElement).files!)}
          />
        </span>
        <div className="xo-form-drop-zone__wrap">
          <div
            className={`${
              files.length ? 'xo-form-drop-zone__list_upload' : 'xo-form-drop-zone__upload'
            }`}
          >
            <div className="xo-form-drop-zone__stack">
              {!files.length ? (
                <>
                  <div className="xo-form-drop-zone__item">
                    <div className="xo-form-drop-zone__action">{hint || 'Add files'}</div>
                  </div>
                  {/* <div className='xo-form-drop-zone__item'>
                  <div className='xo-form-drop-zone__hint'>{hint}</div>
                </div> */}
                </>
              ) : (
                <>
                  {files.map((fileItem, index) => (
                    <div key={index} className="xo-form-drop-zone__icon">
                      <span
                        className={`${
                          type === 'file'
                            ? 'xo-form-drop-zone__stack--file'
                            : 'xo-form-drop-zone__stack--thumbnail'
                        }`}
                      >
                        {
                          type === 'file' && (
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              focusable="false"
                              aria-hidden="true"
                            >
                              <path
                                fill-rule="evenodd"
                                d="M6 11h8v-2h-8v2zm0 4h8v-2h-8v2zm0-8h4v-2h-4v2zm6-5h-6.5a1.5 1.5 0 0 0-1.5 1.5v13a1.5 1.5 0 0 0 1.5 1.5h9a1.5 1.5 0 0 0 1.5-1.5v-10.5l-4-4z"
                              />
                            </svg>
                          )
                          // : <img alt='Alt' src={photoThumbnail} />
                        }
                      </span>
                      <div className="xo-form-drop-zone__file-info">
                        <p>{fileItem.name}</p>
                        <p>{fileItem.size} bytes</p>
                      </div>
                    </div>
                  ))}
                </>
              )}
            </div>
          </div>
        </div>
      </div>
      {error && <Error error={error} />}
      {helpText && <HelpText text={helpText} position="below" />}
    </div>
  );
};

export default DropZone;
