import clsx from 'clsx';
import { ComponentChildren, FunctionComponent, VNode } from 'preact';
import { Error, HelpText, Label } from '../';
import './textfield.scss';

type TProps = {
  label?: string;
  labelHidden?: boolean;
  helpText?: string;
  placeholder?: string;
  value: string | number;
  id?: string;
  name?: string;
  multiline?: boolean;
  className?: string;
  type: string;
  cols?: number;
  rows?: number;
  prefix?: VNode | ComponentChildren;
  suffix?: VNode | ComponentChildren;
  disabled?: boolean;
  required?: boolean;
  iconSearch?: boolean;
  error?: string;
  onChange: (val: string) => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onKeyPress?: (e: KeyboardEvent) => void;
  helpTextPosition?: 'above' | 'below';
};

const TextField: FunctionComponent<TProps> = ({
  label,
  labelHidden,
  value,
  id,
  name,
  type,
  helpText,
  placeholder,
  iconSearch,
  className,
  multiline,
  cols,
  rows,
  prefix,
  suffix,
  error,
  disabled,
  onChange,
  onKeyPress,
  onFocus,
  onBlur,
  helpTextPosition = 'above',
}) => {
  // const [error, setError] = useState<string>(errorMessage);

  const handleChangeValue = (val: string) => {
    onChange(val);

    // if (required && !isEmptyValue(type, value)) {
    //   setError('');
    // } else if (type === 'email' && !isEmailValid(value as string)) {
    //   setError('');
    // }
  };

  const handleChangeValueNumberSegment = (type: 'increase' | 'decrease') => {
    onChange(type === 'increase' ? `${+value + 1}` : `${+value - 1}`);
  };

  const handleKeyPress = (event: KeyboardEvent) => {
    if (type === 'number' && !/[0-9]/.test(event.key)) {
      event.preventDefault();
    }
    if (onKeyPress) {
      onKeyPress(event);
    }
  };

  // const handleBlur = () => {
  //   // For validate
  //   if (required && isEmptyValue(type, value)) {
  //     setError(`${label} is required!`);
  //   } else if (type === 'email' && isEmailValid(value as string)) {
  //     setError(`${label} is invalid email address!`);
  //   }
  //   // For blur event
  //   if (onBlur) {
  //     onBlur();
  //   }
  // };

  return (
    <div
      className={clsx(
        'xo-form-text-field',
        'xo-form-field__main',
        disabled && 'xo-form__field--disabled',
        className,
      )}
      id={`xo-form-field__main--${id}`}
    >
      {label && !labelHidden && <Label label={label} id={id} disabled={disabled || false} />}
      {helpText && helpTextPosition === 'above' && <HelpText text={helpText} position="above" />}
      <div className="xo-form-text-field__connected">
        {multiline ? (
          <div
            className={clsx('xo-form-text-field__wrap', error && 'xo-form-text-field__wrap--error')}
          >
            <textarea
              name={name}
              className="xo-form-text-field__input"
              type="text"
              id={`xo-form__field--${id}`}
              cols={cols}
              rows={rows}
              value={value}
              placeholder={placeholder}
              disabled={disabled}
              // @ts-ignore
              onInput={(e) => handleChangeValue(e.target.value)}
              onBlur={onBlur}
              onFocus={onFocus}
              autocomplete="off"
            >
              {value}
            </textarea>
            <div className="xo-form-text-field__backdrop" />
          </div>
        ) : (
          <div
            className={clsx('xo-form-text-field__wrap', error && 'xo-form-text-field__wrap--error')}
          >
            {iconSearch && (
              <div className="xo-form-text-field__icon">
                <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                  {/* eslint-disable-next-line max-len */}
                  <path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm9.707 4.293-4.82-4.82a5.968 5.968 0 0 0 1.113-3.473 6 6 0 0 0-12 0 6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414z" />
                </svg>
              </div>
            )}
            {prefix && <div className="xo-form-text-field__prefix">{prefix}</div>}
            <input
              name={name}
              className="xo-form-text-field__input"
              type={type}
              id={`xo-form__field--${id}`}
              value={value}
              disabled={disabled}
              placeholder={placeholder}
              // @ts-ignore
              onInput={(e) => handleChangeValue(e.target.value)}
              onKeyPress={(event) => handleKeyPress(event)}
              onBlur={onBlur}
              onFocus={onFocus}
              autocomplete="off"
            />
            {type === 'number' && (
              <div className="xo-form-text-field-spinner">
                <div className="xo-form-text-field-spinner-segment">
                  <div
                    className="xo-form-text-field-spinner-icon"
                    onClick={() => {
                      handleChangeValueNumberSegment('increase');
                    }}
                  >
                    <svg
                      viewBox="0 0 20 20"
                      class="Icon_Icon__Dm3QW"
                      style="width: 20px; height: 20px;"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M6.24 11.8a.75.75 0 0 0 1.06-.04l2.7-2.908 2.7 2.908a.75.75 0 1 0 1.1-1.02l-3.25-3.5a.75.75 0 0 0-1.1 0l-3.25 3.5a.75.75 0 0 0 .04 1.06Z"
                      ></path>
                    </svg>
                  </div>
                </div>
                <div className="xo-form-text-field-spinner-segment">
                  <div
                    className="xo-form-text-field-spinner-icon"
                    onClick={() => {
                      handleChangeValueNumberSegment('decrease');
                    }}
                  >
                    <svg
                      viewBox="0 0 20 20"
                      class="Icon_Icon__Dm3QW"
                      style="width: 20px; height: 20px;"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M6.24 8.2a.75.75 0 0 1 1.06.04l2.7 2.908 2.7-2.908a.75.75 0 1 1 1.1 1.02l-3.25 3.5a.75.75 0 0 1-1.1 0l-3.25-3.5a.75.75 0 0 1 .04-1.06Z"
                      ></path>
                    </svg>
                  </div>
                </div>
              </div>
            )}
            {suffix && <div className="xo-form-text-field__suffix">{suffix}</div>}
            <div className="xo-form-text-field__backdrop" />
          </div>
        )}
      </div>
      {error && <Error error={error} />}
      {helpText && helpTextPosition === 'below' && <HelpText text={helpText} position="below" />}
    </div>
  );
};

export default TextField;
