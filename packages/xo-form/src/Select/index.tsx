import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { Error, HelpText, Label, TextField } from '..';
import { t } from '../helpers/translate';
import { OtherValue, TOptionsSelect } from '../types/field';
import './select.scss';

type TProps = {
  id?: string;
  options: TOptionsSelect[];
  label: string;
  helpText?: string;
  labelHidden?: boolean;
  disabled?: boolean;
  name?: string;
  value?: string;
  className?: string;
  required?: boolean;
  otherValue?: OtherValue;
  placeholder?: string;
  error?: string;
  onBlur?: (e: any) => void;
  onChange: (value: string) => void;
};

const Select: FunctionComponent<TProps> = ({
  id,
  label,
  labelHidden,
  options,
  value,
  className,
  helpText,
  disabled,
  otherValue,
  error,
  onBlur,
  placeholder,
  onChange,
}) => {
  const [otherText, setOtherText] = useState<string>('');
  const [selectOther, setSelectOther] = useState<boolean>(false);
  const [valueSelected, setValueSelected] = useState<string>('');

  useEffect(() => {
    if (value) {
      const valueSelect = options.find((option) => option.value === value)?.label;
      setValueSelected(
        t(valueSelect) || (otherValue?.allowOtherValue && t(otherValue?.value)) || '',
      );
    }
  }, [value]);

  const handleOnChange = (val: string) => {
    if (!disabled) {
      if (val === 'other') {
        setValueSelected(t(otherValue?.value));
        setSelectOther(true);
        onChange(otherText);
      } else {
        setSelectOther(false);
        onChange(val);
      }
    }
  };

  const handleOtherChange = (value: string) => {
    onChange(value);
    setOtherText(value);
  };

  return (
    <div
      className={clsx('xo-form-select', disabled && 'xo-form__field--disabled', className)}
      id={`xo-form-field__main--${id}`}
    >
      {!labelHidden && <Label id={id} label={label} />}
      <div className={clsx('xo-form-select__select', error && 'xo-form-select--error')}>
        <select
          id={id}
          className="xo-form-select__input"
          onChange={(e) => handleOnChange((e.target as HTMLInputElement).value)}
          disabled={disabled}
          onBlur={onBlur}
          placeholder={placeholder}
          value=""
        >
          {options.map((option) => (
            <option key={option.value} value={option.value} disabled={option?.disabled}>
              {t(option.label)}
            </option>
          ))}
          {otherValue?.allowOtherValue && <option value="other">{t(otherValue!.value)}</option>}
        </select>
        <div className="xo-form-select__content">
          <span className="xo-form-select__option">{valueSelected}</span>
          <span>
            <span className="xo-form-select__icon">
              <span />
              <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                {/* eslint-disable-next-line max-len */}
                <path d="M7.676 9h4.648c.563 0 .879-.603.53-1.014l-2.323-2.746a.708.708 0 0 0-1.062 0l-2.324 2.746c-.347.411-.032 1.014.531 1.014Zm4.648 2h-4.648c-.563 0-.878.603-.53 1.014l2.323 2.746c.27.32.792.32 1.062 0l2.323-2.746c.349-.411.033-1.014-.53-1.014Z" />
              </svg>
            </span>
          </span>
        </div>
        <div className="xo-form-select__backdrop" />
      </div>
      {selectOther && (
        <div className="xo-form-select--other">
          <TextField
            label="Other value"
            type="text"
            value={otherText}
            onChange={handleOtherChange}
          />
        </div>
      )}
      {error && <Error error={error} />}
      {helpText && <HelpText text={helpText} position="below" />}
    </div>
  );
};

export default Select;
