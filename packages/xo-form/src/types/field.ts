export type TMultiLanguage = {
  [key: string]: string;
};

export interface IFieldCommon {
  name?: string;
  id?: string;
  label: string;
  labelHidden?: boolean;
  required?: boolean;
  helpText?: string;
  value?: string;
  onBlur?: any;
}

export type TOptionsSelect = {
  value: string,
  label: Record<string, string>,
  disabled?: boolean,
};

export type TChoice = {
  label: TMultiLanguage | string,
  value: string,
  id?: string,
  disabled?: boolean,
  helpText?: string,
};

export type OtherValue = {
  allowOtherValue: boolean;
  value: Record<string, string>;
};
