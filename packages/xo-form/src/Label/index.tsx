import clsx from 'clsx';
import { FunctionComponent } from 'preact';
import './label.scss';

type TProps = {
  label?: string;
  id?: string;
  required?: boolean;
  disabled?: boolean;
};

const Label: FunctionComponent<TProps> = ({ label, id, required, disabled }) => (
  <>
    {label && (
      <div
        className={clsx(
          'xo-form-label',
          `xo-form-label--${id}`,
          disabled && 'xo-form-label--disabled',
        )}
      >
        <label className="xo-form-label__text" for={id}>
          <span>{label}</span>
          {required && <span className="xo-form-label__required">*</span>}
        </label>
      </div>
    )}
  </>
);

export default Label;
