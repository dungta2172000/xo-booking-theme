import { useEffect, useReducer, useState } from 'preact/hooks';
import { useStoreType, Hook, Store } from 'teaful/dist/types';

type TAction = {
  type: 'LOADING' | 'FETCHED' | 'ERROR';
  payload?: any;
};

type TState = {
  status: 'loading' | 'fetched' | 'error';
  error: any;
};

export const useFetch = <StoreType extends Store>(
  url: string,
  useStore?: Hook<StoreType> & useStoreType<StoreType>,
) => {
  //set data fetch to store (if provide) or create new state if not recieve store
  const [data, setData] = useStore ? useStore() : useState();

  const initialState: TState = {
    status: 'loading',
    error: null,
  };

  const [state, dispatch] = useReducer((state: any, action: TAction): TState => {
    switch (action.type) {
      case 'LOADING':
        return { ...initialState, status: 'loading' };
      case 'FETCHED':
        return { ...initialState, status: 'fetched' };
      case 'ERROR':
        return { ...initialState, status: 'error', error: action.payload };
      default:
        return state;
    }
  }, initialState);

  useEffect(() => {
    let cancelRequest = false;
    if (!url) return;

    const fetchData = async () => {
      dispatch({ type: 'LOADING' });
      if (
        (typeof data === 'object' && Object.keys(data).length > 0) ||
        (typeof data === 'string' && data != null)
      ) {
        //check if data already exist
        dispatch({ type: 'FETCHED' });
      } else {
        try {
          const response = await fetch(`${url}`);



          const resData = await response.json();

          setData(resData);
          if (cancelRequest) {
            return;
          }
          dispatch({ type: 'FETCHED' });
        } catch (error: any) {
          if (cancelRequest) return;
          dispatch({ type: 'ERROR', payload: error.message });
        }
      }
    };

    fetchData();
    return function cleanup() {
      cancelRequest = true; //avoid update when component has been unmounted
    };
  }, [url]);

  return { data, state, url };
};
