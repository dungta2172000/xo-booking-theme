export type TMultiLanguage = {
  [key: string]: string;
};

export const t = (text?: TMultiLanguage): string => {
  if (!text || typeof text === 'undefined') {
    return '';
  }

  if (typeof text === 'string') {
    return text;
  }

  if (text[Shopify.locale]) {
    return text[Shopify.locale];
  }

  if (typeof xoDfLang !== 'undefined' && xoDfLang && text[xoDfLang]) {
    return text[xoDfLang];
  }
  return text[Object.keys(text)[0]];
};
