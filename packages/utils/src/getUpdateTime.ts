export const getUpdateTime = (): number => {
  let time = new Date().getTime();
  if (typeof xoUpdate !== 'undefined') {
    time = xoUpdate;
  }
  return time;
};
