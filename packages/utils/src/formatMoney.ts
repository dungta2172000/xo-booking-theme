type NumString = number | string;

export const formatMoney = (cents: NumString): string => {
  let formatString = '${{amount}}';

  if (typeof xoMoneyFormat !== 'undefined' && xoMoneyFormat !== '') {
    formatString = xoMoneyFormat;
  }

  if (typeof cents === 'string') {
    cents = cents.replace('.', '');
  }
  let value = '';
  const placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;

  function defaultOption(opt: NumString, def: NumString): NumString {
    return typeof opt == 'undefined' ? def : opt;
  }

  function formatWithDelimiters(
    number: any,
    precision: number,
    thousands?: string,
    decimal?: string,
  ): string {
    precision = defaultOption(precision, 2) as number;
    thousands = defaultOption(thousands!, ',') as string;
    decimal = defaultOption(decimal!, '.') as string;

    if (isNaN(number) || number == null) {
      return '0';
    }

    number = (number / 100.0).toFixed(precision);

    const parts = number.split('.');
    const dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands);
    const cents1 = parts[1] ? decimal + parts[1] : '';

    return dollars + cents1;
  }

  switch (formatString.match(placeholderRegex)![1]) {
    case 'amount':
      value = formatWithDelimiters(cents, 2);
      break;
    case 'amount_no_decimals':
      value = formatWithDelimiters(cents, 0);
      break;
    case 'amount_with_comma_separator':
      value = formatWithDelimiters(cents, 2, '.', ',');
      break;
    case 'amount_no_decimals_with_comma_separator':
      value = formatWithDelimiters(cents, 0, '.', ',');
      break;
    case 'amount_with_apostrophe_separator':
      value = formatWithDelimiters(cents, 2, "'", '.');
      break;
    default:
      value = formatWithDelimiters(cents, 2);
      break;
  }

  return formatString.replace(placeholderRegex, value);
};
