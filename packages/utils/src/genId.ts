export const genId = (prefix: string = 'xo-') => `${prefix}${Math.random().toString(36).substring(2, 9)}`;
