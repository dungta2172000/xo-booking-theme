import { ComponentChildren, FunctionalComponent } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import './collapsible.scss';

interface CollapsibleProps {
  isOpen: boolean;
  children: ComponentChildren;
  transitionDuration?: number;
  id?: string;
}

const Collapsible: FunctionalComponent<CollapsibleProps> = ({
  isOpen,
  children,
  transitionDuration = 500,
  id,
}: CollapsibleProps) => {
  const childRef = useRef<any>(null);
  const [childHeight, setChildHeight] = useState<number>(0);

  useEffect(() => {
    // get child component height before it render
    const childrenHeight = childRef.current?.clientHeight;

    // for component not track animation when first load site
    setTimeout(() => {
      setChildHeight(childrenHeight);
    }, 500);
  }, []);

  const divProps: any = {};
  if (id) {
    divProps.id = id;
  }

  return (
    <div
      {...divProps}
      className={'xo-ui-collapsible'}
      data-state={isOpen ? 'open' : 'closed'}
      style={{
        animationDuration: `${transitionDuration}ms`,
        '--xo-ui-collapsible-content-height': `${childHeight}px`,
      }}
    >
      <div ref={childRef}>{children}</div>
    </div>
  );
};

export default Collapsible;
