import React, { FunctionComponent } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import './index.scss';

type TProps = {
  content: string,
  duration?: number,
  error?: boolean,
  onDismiss: () => void,
};

const Toast: FunctionComponent<TProps> = ({
  content, duration, error, onDismiss,
}) => {
  const toastRef = useRef<HTMLDivElement>({} as any);
  const [show, setShow] = useState<boolean>(false);
  const [translatePosition, setTranslatePosition] = useState<React.JSX.CSSProperties>();

  useEffect(() =>{
    if (show) {
      setTranslatePosition({
        '--pc-toast-manager-translate-y': `${show ? -64 : 40}px`,
      })
    } else {
      setTranslatePosition({
        '--pc-toast-manager-translate-y': `${show ? 0 : 40}px`,
      })
    }
  }, [show]);

  const handleDismiss = () => {
    setTimeout(() => {
      onDismiss();
    }, 400);
    setShow(false);
  };

  useEffect(() => {
    setShow(true);
    const timeShow = setTimeout(() => {
      handleDismiss();
    }, duration || 4000);
    return () => clearTimeout(timeShow);
  }, []);

  return (
    <div className='xo-ui-toast'>
      <div className='xo-ui-toast__manager'>
        <div className='xo-ui-toast__manager--wrap' ref={toastRef} style={translatePosition}>
          <div className={`xo-ui-toast__frame ${error ? 'xo-ui-toast__frame--error' : ''}`}>
            {error && <div className='xo-ui-toast__icon--error'>
              {/* eslint-disable-next-line max-len */}
              <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-9a1 1 0 0 0 2 0v-2a1 1 0 1 0-2 0v2zm0 4a1 1 0 1 0 2 0 1 1 0 0 0-2 0z" /></svg>
            </div>}
            <div className='xo-ui-toast__frame--inline'>
              <span>{content}</span>
            </div>
            <div className='xo-ui-toast__frame--close' onClick={handleDismiss}>
              <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                {/* eslint-disable-next-line max-len */}
                <path d="M6.707 5.293a1 1 0 0 0-1.414 1.414l3.293 3.293-3.293 3.293a1 1 0 1 0 1.414 1.414l3.293-3.293 3.293 3.293a1 1 0 0 0 1.414-1.414l-3.293-3.293 3.293-3.293a1 1 0 0 0-1.414-1.414l-3.293 3.293-3.293-3.293Z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Toast;
