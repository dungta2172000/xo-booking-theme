import { FunctionComponent } from 'preact';
import clsx from 'clsx';
import Spinner from '../Spinner';
import './button.scss';

type TProps = {
  children?: string,
  primary?: boolean,
  secondary?: boolean,
  tertiary?: boolean,
  className?: string,
  loading?: boolean,
  disabled?: boolean,
  fullWidth?: boolean,
  disclosure?: boolean,
  onClick: () => void,
};

const Button: FunctionComponent<TProps> = ({
  className, children, primary, secondary, tertiary, fullWidth,
  loading, disabled, onClick, disclosure,
}) => {
  let btnClassName = 'xo-ui-btn';
  if (className) {
    btnClassName = className;
  }

  const handleClick = () => {
    if (!disabled && !loading) {
      onClick();
    }
  };

  return (
    <div
      className={clsx(
        btnClassName,
        primary && `${btnClassName}--primary`,
        !primary && secondary && `${btnClassName}--secondary`,
        !primary && !secondary && tertiary && `${btnClassName}--tertiary`,
        disabled && `${btnClassName}--disabled`,
        loading && `${btnClassName}--loading`,
        fullWidth && `${btnClassName}--full-width`,
      )}
      onClick={handleClick}
    >
      <button>
        <span className={loading ? 'loading_text' : ''}>{children}</span>
        {loading && <div className="xo-ui-btn__loading"><Spinner primary={primary} size='small' /></div>}
        {disclosure && <span className={primary ? 'icon_primary' : 'icon'}>
          <svg viewBox="0 0 20 20" focusable="false" aria-hidden="true">
            <path d="M13.098 8h-6.196c-.751 0-1.172.754-.708 1.268l3.098 3.432c.36.399 1.055.399 1.416 0l3.098-3.433c.464-.513.043-1.267-.708-1.267Z" />
          </svg>
        </span>}
        </button>
    </div>
  );
};

export default Button;
