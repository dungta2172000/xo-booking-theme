import { FunctionComponent, VNode } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import clsx from 'clsx';
import './popover.scss';

type TProps = {
  id?: string,
  active: boolean,
  activator: VNode,
  onClose: () => void,
  children: any,
};

const Popover: FunctionComponent<TProps> = ({
  active, activator, onClose, children, id,
}) => {
  const [activePopover, setActivePopover] = useState<boolean>(false);
  const [orient, setOrient] = useState<'top' | 'bottom'>('bottom');
  const childRef = useRef<HTMLDivElement>(null);
  const mainRef = useRef<HTMLDivElement>(null);
  const popoverRef = useRef<HTMLDivElement>(null);
  const buttonRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const childHeight = childRef.current?.offsetHeight!;
    // eslint-disable-next-line no-unsafe-optional-chaining
    const distanceFromBottom = window.innerHeight - mainRef.current?.getBoundingClientRect().top! - buttonRef.current?.offsetHeight!;
    const oriented = (distanceFromBottom > 12 + childHeight) ? 'bottom' : 'top';
    setOrient(oriented);
  }, [activePopover, childRef.current?.offsetHeight]);

  const handleClose = () => {
    const popoverContainer = document.getElementById(id || 'expopover') as HTMLElement;
    if (popoverContainer) {
      popoverContainer.classList.add('xo-ui-popover__close');
      setTimeout(() => {
        onClose();
        setActivePopover(false);
      }, 400);
    }
  };

  useEffect(() => {
    const closeOpenPopover = (e: MouseEvent) => {
      // Check click outside popover and button
      if (popoverRef.current && !popoverRef.current.contains(e.target as Node)
      && buttonRef.current && !buttonRef.current.contains(e.target as Node)
      ) {
        onClose();
      }
    };

    document.addEventListener('mousedown', closeOpenPopover);
    return () => document.removeEventListener('mousedown', closeOpenPopover);
  }, []);

  useEffect(() => {
    if (active) {
      setActivePopover(active);
    } else {
      handleClose();
    }
  }, [active]);

  return (
    <div className="xo-ui-popover" ref={mainRef}>
      <div ref={buttonRef}>{activator}</div>
      <div
        id={id || 'expopover'}
        className={clsx(
          'xo-ui-popover__wrap',
          activePopover && 'xo-ui-popover__wrap--open',
          orient === 'bottom' && 'xo-ui-popover__wrap--bottom',
          orient === 'top' && 'xo-ui-popover__wrap--top',
        )}
        ref={popoverRef}
      >
        <div ref={childRef} className="xo-ui-popover__content">
          {activePopover && children}
        </div>
      </div>
    </div>
  );
};

export default Popover;
