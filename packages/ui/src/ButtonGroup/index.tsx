import { FunctionComponent } from 'preact';
import clsx from 'clsx';
import './buttonGroup.scss';

type TProps = {
  spacing?: 'extraTight' | 'tight' | 'loose',
  segmented?: boolean,
  children?: any,
  fullWidth?: boolean,
};

const ButtonGroup: FunctionComponent<TProps> = ({ children, segmented, spacing }) => (
  <div className={clsx(
    'xo-ui-button-group',
    segmented && 'xo-ui-button-group--segmented',
    !segmented && spacing && `xo-ui-button-group--${spacing}`,
  )}>
    {children}
  </div>
);

export default ButtonGroup;
