export type TAction = {
  content: string,
  loading?: boolean,
  onAction: () => void,
};
