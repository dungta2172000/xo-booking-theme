export type TItemActionList = {
  id?: string,
  content: string,
  helperText?: string,
  onAction: () => void,
  disabled?: boolean,
};