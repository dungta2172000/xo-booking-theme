module.exports = {
  root: true,
  extends: ["preact"],
  parserOptions: {
    project: "./tsconfig.json",
    tsconfigRootDir: __dirname
  },
  rules: {
    "import/no-extraneous-dependencies": "off",
    "linebreak-style": 0
  }
};
