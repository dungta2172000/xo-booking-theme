module.exports = {
  env: {
    "browser": true,
    "es2021": true
  },
  extends: [
    "airbnb-base",
    "airbnb-typescript/base",
    "plugin:import/recommended",
    "plugin:import/typescript"
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module"
  },
  rules: {
    "import/prefer-default-export": "off",
    "max-len": ["error", { "code": 180 }],
  },
}
