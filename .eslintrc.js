module.exports = {
  root: true,
  // This tells ESLint to load the config from the package `eslint-config-preact`
  extends: ['preact'],
  settings: {
    next: {
      rootDir: ['apps/*/'],
    },
  },
};
