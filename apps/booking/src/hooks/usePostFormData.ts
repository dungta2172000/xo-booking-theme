import { useCallback } from 'preact/hooks';

export const usePostFormData = () => {
  // TODO: xử lý submit lỗi
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const postFormData = async (
    submitFormData: FormData,
    uri: string,
    headers: Record<string, string>,
    // eslint-disable-next-line consistent-return
  ) => {
    const options = {
      method: 'post',
      body: submitFormData,
      headers,
    };

    try {
      const res = await fetch(uri, options);
      return res;
    } catch (e) {
      console.log(e);
      return null;
    }
  };

  return useCallback(
    async (data: any, uri: string, headers = {}) => {
      const mutationResult = await postFormData(data, uri, headers);
      return mutationResult;
    },
    [postFormData],
  );
};
