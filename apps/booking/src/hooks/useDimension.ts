import { debounce } from '@helper/debounce';
import { useEffect, useState } from 'preact/hooks';

export const getWindowDimension = () => {
  const { innerWidth: width, innerHeight: height } = window;

  return {
    width,
    height,
  };
};

export const useDimension = () => {
  const [dimension, setDimension] = useState(getWindowDimension());

  useEffect(() => {
    const updateSize = debounce(() => {
      setDimension(getWindowDimension());
    }, 500);

    window.addEventListener('resize', updateSize);

    return () => {
      window.removeEventListener('resize', updateSize);
    };
  }, []);

  return dimension;
};
