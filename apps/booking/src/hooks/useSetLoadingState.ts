import { useEffect } from 'preact/hooks';
import { useButtonStatus } from '@store/index';

export const useSetLoadingState = (status: 'loading' | 'fetched' | 'error') => {
  const [, setDisableFooter] = useButtonStatus.disable();

  useEffect(() => {
    if (status === 'loading') {
      setDisableFooter(true);
    } else {
      setDisableFooter(false);
    }
  }, [status]);
};
