import { convertToFormData } from '@helper/form';
import { useButtonStatus, useDataSubmit, useForms } from '@store/index';
import { useCallback } from 'preact/hooks';
import { TResRequest } from 'types/data/general';
import { usePostFormData } from './usePostFormData';

export const useFormSubmit = () => {
  const postFormData = usePostFormData();
  const [dataSubmit] = useDataSubmit();
  const [form] = useForms();
  const [, setButtonLoading] = useButtonStatus.loading();

  const submitForm = async (): Promise<TResRequest> => {
    // submit form
    const submitData = convertToFormData({
      ...dataSubmit.form,
      id: form.id as unknown as string,
    });

    const infoFormResponse = await postFormData(
      submitData,
      `${import.meta.env.VITE_APP_ENDPOINT_PROD}/form/submit`,
    );

    const res: TResRequest = { data: null, error: null };

    if (infoFormResponse?.status !== 200) {
      res.error = JSON.stringify({ title: 'Fail', description: 'Submit form failed' });
    } else {
      const data = await infoFormResponse.json();
      res.data = data;
    }

    return res;
  };

  return useCallback(async () => {
    setButtonLoading(true);
    const mutationResult = await submitForm();
    const err = mutationResult.error;

    if (err) {
      throw new Error(err);
    }
    setButtonLoading(false);
    return mutationResult;
  }, [submitForm, dataSubmit]);
};
