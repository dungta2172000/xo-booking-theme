/* eslint-disable no-undef */
import { useCallback } from 'preact/hooks';
import dayjs from 'dayjs';
import tz from 'dayjs/plugin/timezone';
import { checkCartItems } from '@helper/cart';
import { useButtonStatus, useDataSubmit } from '@store/index';
import { TResRequest } from 'types/data/general';
import { usePostFormData } from './usePostFormData';

dayjs.extend(tz);

export const useOrderSubmit = () => {
  const [, setButtonLoading] = useButtonStatus.loading();
  const postFormData = usePostFormData();
  const [dataSubmit] = useDataSubmit();
  const { service } = dataSubmit;

  const submitOrder = async (dataFormId: string): Promise<TResRequest> => {
    const resReturn: TResRequest = {
      data: null,
      error: null,
    };
    const currentCustomerTimezone = dayjs.tz.guess();
    const timeStart = dataSubmit.timeStart as Date;
    const timeEnd = new Date(
      new Date(timeStart).setMinutes(new Date(timeStart).getMinutes() + (service?.duration || 0)),
    );

    if (service?.settings.skipCheckout) {
      // prepare customer info
      const isCustomer = typeof xoCustomerId !== 'undefined' && xoCustomerId !== '';
      const customerData = isCustomer ? { id: Number(xoCustomerId) } : dataSubmit.customer;

      const orderData = {
        customer: customerData,
        date: {
          customerTimezone: currentCustomerTimezone,
          start: timeStart,
          end: timeEnd,
        },
        productId: service.product.id,
        variantId: dataSubmit.variantId,
        dataFormId,
        frontSiteLocale: Shopify.locale,
      };

      const serviceOrderResponse = await postFormData(
        JSON.stringify(orderData),
        `${import.meta.env.VITE_APP_ENDPOINT_PROD}/service-order`,
        { 'Content-Type': 'application/json' },
      );

      const serviceData = await serviceOrderResponse?.json();

      if (serviceOrderResponse?.status !== 200) {
        const errorMsg = JSON.parse(JSON.parse(serviceData?.message))
          .map((item: any) => item.message)
          .join('<br/>');
        resReturn.error = JSON.stringify({
          title: serviceOrderResponse?.status ?? 'Failed',
          description: errorMsg,
        });
      } else {
        setTimeout(() => {
          window.location.replace(`https://${Shopify.shop}`);
        }, 3000);
      }
    } else {
      const isCartItemDuplicated = await checkCartItems(
        dataSubmit.variantId as number,
        dataSubmit.timeStart as Date,
      );
      if (!isCartItemDuplicated) {
        const bookingProperties = {
          timeStart: timeStart.toString(),
          timeEnd: timeEnd.toString(),
          customerTimezone: currentCustomerTimezone,
          dataFormId,
          frontSiteLocale: Shopify.locale,
        };

        try {
          const submitData = JSON.stringify({
            items: [
              {
                id: dataSubmit.variantId,
                quantity: 1,
                properties: bookingProperties,
              },
            ],
          });

          const headers = { 'Content-Type': 'application/json' };
          await postFormData(submitData, '/cart/add.js', headers);
        } catch (e: any) {
          resReturn.error = JSON.stringify({ title: 'Fail', description: 'Add to cart fail' });
        }
      }
      window.location.href = '/checkout';
    }

    return resReturn;
  };

  return useCallback(
    async (dataFormId: string) => {
      setButtonLoading(true);
      const mutationResult = await submitOrder(dataFormId);
      const err = mutationResult.error;
      if (err) {
        setButtonLoading(false);
        throw new Error(err);
      }
      if (service?.settings.skipCheckout) {
        setButtonLoading(false);
      }
      return mutationResult;
    },
    [submitOrder, dataSubmit],
  );
};
