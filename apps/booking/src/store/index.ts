import createStore from 'teaful';
import { ISettings } from '../../types/data/settings';
import { IService } from '../../types/data/service';
import { ICustomAttributeSubmit } from '../../types/data/submit';
import { IForm } from '../../types/data/form';
import { IToast, TToastType } from '../../types/data/general';

export interface IStore {
  maxStep: number;
  currentStep: number;
}

export const { useStore: useDataSubmit } = createStore<ICustomAttributeSubmit>({
  timeEnd: null,
  timeStart: null,
  customerTimezone: null,
  dataFormId: null,
  service: null,
  customer: {
    firstName: '',
    lastName: '',
    email: '',
  },
  form: {},
  variantId: null,
});

export const { useStore } = createStore<Record<string, IStore>>();

export const { useStore: useSetting } = createStore<ISettings>();

export const { useStore: useServices } = createStore<[IService]>();

export const { useStore: useSchedules } = createStore({}, ({}) => {});

export const { useStore: useForms } = createStore<IForm>();

export const { useStore: useFormErrors } = createStore<Record<string, string>>({});

export const { useStore: useToast } = createStore<IToast>({
  show: false,
  title: '',
  description: '',
  type: TToastType.success,
  position: 'bottom-right',
});

type TButtonStatus = {
  disable: boolean;
  loading: boolean;
};
export const { useStore: useButtonStatus } = createStore<TButtonStatus>({
  disable: false,
  loading: false,
});
