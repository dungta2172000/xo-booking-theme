import React, { createContext } from 'preact';
import createStore from 'teaful';
import { useStoreType } from 'teaful/dist/types';
import { IStore } from '.';

interface IContext {
  useStore: useStoreType<IStore>;
}

interface IProps {
  id: string;
  children: React.ComponentChildren;
}

// @ts-ignore
export const AppContext = createContext<IContext>(null);

export const AppProvider: React.FunctionComponent<IProps> = ({ children }: IProps) => {
  const { useStore } = createStore({
    maxStep: 1,
    currentStep: 1,
  });

  return (
    <AppContext.Provider
      value={{
        useStore,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
