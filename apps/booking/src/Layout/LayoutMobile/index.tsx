import Footer from '@components/Footer';
import Header from '@components/Header';
import StepDetail from '@components/StepDetail';
import Steps from '@components/Steps';
import Toast from '@components/Toast';
import './layoutMobile.scss';

const LayoutMobile = () => (
  <div className="xobk-layout-mobile">
    <Header />
    <Steps />
    <div className="xobk-layout-mobile__body">
      <StepDetail />
    </div>
    <div className="xobk-layout-mobile__footer">
      <Footer />
    </div>
    <Toast />
  </div >
);

export default LayoutMobile;
