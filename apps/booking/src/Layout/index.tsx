import { useDimension } from '@hooks/useDimension';
import Footer from '../components/Footer';
import Header from '../components/Header';
import StepDetail from '../components/StepDetail';
import Steps from '../components/Steps';
import Toast from '../components/Toast';
import LayoutMobile from './LayoutMobile';
import './layout.scss';

const Layout = () => {
  const isMobile = useDimension().width < 768;

  if (isMobile) {
    return <LayoutMobile />;
  }

  return (
    <div className="xobk-layout">
      <Steps />
      <div className="xobk-layout__right">
        <div className="xobk-layout__header">
          <Header />
        </div>
        <div className="xobk-layout__body">
          <StepDetail />
        </div>
        <div className="xobk-layout__footer">
          <Footer />
        </div>
      </div>
      <Toast />
    </div>
  );
};

export default Layout;
