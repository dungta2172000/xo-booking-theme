export const postProxyUrl = (uri: string) => `${import.meta.env.VITE_APP_ENDPOINT_PROD}/${uri}${
  import.meta.env.VITE_NODE_ENV === 'development' ? '.json' : ''
}`;

export const DEFAULT_SERVICE_THUMBNAIL = 'https://cdn.xotiny.com/assets/img/xo-booking-df.png';
