import { Component, ComponentChildren, FunctionalComponent } from 'preact';
import { useMemo } from 'preact/hooks';
import { Button } from '@xo/ui';
import './card.scss';

interface ICardProps {
  children: ComponentChildren;
  backgroundColor?: string;
  borderColor?: string;
  sectioned?: boolean;
  title?: Component | string;
  color?: string;
  action?: {
    content: string;
    onAction: () => void;
  };
}

const Card: FunctionalComponent<ICardProps> = ({
  children,
  sectioned,
  title,
  backgroundColor,
  borderColor,
  color,
  action,
}: ICardProps) => {
  const titleComponent = useMemo(() => {
    if (title) {
      if (typeof title === 'string') {
        return (
          <div className={action ? 'xobk-stack__item xobk-stack__item--fill' : ''}>
            <h2 className={'xobk-heading'}>{title}</h2>
          </div>
        );
      }
      return <div className={action ? 'xobk-stack__item--fill' : ''}>{title}</div>;
    }
    return null;
  }, [title, action]);

  return (
    <div
      className="xobk-card"
      style={{
        backgroundColor: backgroundColor || '#fff',
        border: `2px solid ${borderColor || '#000'}`,
        color: color || '#fff',
      }}
    >
      <div className="xobk-card__header">
        <div className={action ? 'xobk_stack xobk_stack--alignmentBaseline' : ''}>
          {titleComponent}
          {action && (
            <div className={action ? 'xobk-stack__item' : ''}>
              <Button onClick={() => { }}>
                {action.content}
              </Button>
            </div>
          )}
        </div>
      </div>
      {sectioned ? <div className="xobk-card_section">{children}</div> : <div>{children}</div>}
    </div>
  );
};

export default Card;
