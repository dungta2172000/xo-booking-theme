import { FunctionalComponent } from 'preact';
import { useContext } from 'preact/hooks';
import { t } from '@xo/utils2/src/translate';
import { getServiceInProductPage } from '@helper/service';
import { useDimension } from '@hooks/useDimension';
import { AppContext } from '@store/context';
import { useServices, useSetting } from '@store/index';
import StepProcess from '@asset/StepProcess';
import './stepper.scss';
import { TLabelNaming } from 'types/data/settings';

const Steps: FunctionalComponent = () => {
  const { useStore } = useContext(AppContext);
  const isMobile = useDimension().width < 768;
  const [setting] = useSetting();
  const [currentStep, setCurrentStep] = useStore.currentStep();
  const [maxStep] = useStore.maxStep();
  const [serviceList] = useServices();

  if (isMobile) {
    const thumbColor = setting.storeLayout.colorTheme;
    const progressBarColor = `${setting!.storeLayout.colorTheme}B3`;

    return (
      <div className="xobk-steps-mobile">
        <StepProcess
          step={currentStep}
          progressBarColor={progressBarColor}
          thumbColor={thumbColor}
        />
      </div>
    );
  }

  const getStepDisable = (index: number) => {
    if (index === 0) {
      const serviceDetailInProductPage = getServiceInProductPage(serviceList);

      if (
        serviceDetailInProductPage &&
        index === 0 &&
        serviceDetailInProductPage?.product?.variants?.length <= 1
      ) {
        return true;
      }
      return false;
    }

    if (index + 1 > maxStep) {
      return true;
    }

    return false;
  };

  return (
    <ul className="xobk-steps">
      {(Object.keys(setting.translation.progress) as Array<keyof TLabelNaming['progress']>).map(
        (step, index) => (
          <li
            onClick={() => {
              if (index + 1 <= maxStep) {
                setCurrentStep(index + 1);
              }
            }}
            key={index}
            className={`xobk-steps__step ${
              index + 1 === currentStep ? 'xobk-steps__step--active' : ''
            } ${getStepDisable(index) ? 'xobk-steps__step--disable' : ''}`}
          >
            {index + 1 < 10 ? `0${index + 1}` : index + 1}.{' '}
            {t(setting.translation.progress![step].title)}
          </li>
        ),
      )}
    </ul>
  );
};

export default Steps;
