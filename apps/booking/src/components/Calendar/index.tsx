import { FunctionComponent } from 'preact';
import { useCallback, useEffect, useMemo, useState } from 'preact/hooks';
import dayjs, { Dayjs } from 'dayjs';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import weekOfYear from 'dayjs/plugin/weekOfYear';
import objectSupport from 'dayjs/plugin/objectSupport';
import { t } from '@xo/utils2/src/translate';
import { WEEK_DAYS_SHORT, MONTHS, WEEK_DAYS_LONG, MONTH_SHORT } from '@constants/index';
import LeftArrow from '@asset/LeftArrow';
import RightArrow from '@asset/RightArrow';
import { useSetting } from '@store/index';
import './styles.scss';
import { TMonth, TWeekDayLowerCase } from 'types/data/settings';

const DAY_HEADER = [0, 1, 2, 3, 4, 5, 6];

dayjs.extend(isSameOrBefore);
dayjs.extend(weekOfYear);
dayjs.extend(objectSupport);

type TCalendarProps = {
  active: Dayjs | null;
  onDayClick: (day: Dayjs | null) => void;
  disableDates: (day: Dayjs) => boolean;
  monthNames?: string[];
  longDayNames?: string[];
  shortDayNames?: string;
};

const Calendar: FunctionComponent<TCalendarProps> = ({
  active,
  monthNames,
  longDayNames,
  shortDayNames,
  onDayClick,
  disableDates,
}: TCalendarProps) => {
  const now = dayjs();

  const [state, setState] = useState({
    month: active ? active.month() : now.month(),
    year: active ? +active.year() : +now.year(),
    today: now.set('hour', 0).set('minute', 0).set('second', 0),
  });
  const { month, year, today } = state;

  const [settings] = useSetting();
  const { month: monthLabel, weekday: weekdayLabel } = settings.translation.calendar;

  useEffect(() => {
    let [newMonth, newYear] = [month, year];
    if (active !== null) {
      if (month !== active.month()) newMonth = active.month();
      if (year !== active.year()) newYear = active.year();
      setState({ ...state, month: newMonth, year: newYear });
    }
  }, [active]);

  const translateWeekday = useMemo(() => {
    if (weekdayLabel)
      return WEEK_DAYS_SHORT.map((day) => t(weekdayLabel[day.toLowerCase() as TWeekDayLowerCase]));
    return WEEK_DAYS_SHORT;
  }, [weekdayLabel]);

  const translateMonth = useMemo(() => {
    if (monthLabel)
      return MONTH_SHORT.map((monthTemp) => t(monthLabel[monthTemp.toLowerCase() as TMonth]));
    return MONTHS;
  }, [monthLabel]);

  const daysInMonth = useMemo(() => {
    const daysReturn = [];

    const lastDayInMonth = dayjs({ month }).endOf('month');
    let dayTemp = dayjs({ month }).startOf('month');

    while (dayTemp.isSameOrBefore(lastDayInMonth)) {
      daysReturn.push(dayTemp);
      dayTemp = dayTemp.add(1, 'day');
    }
    return daysReturn;
  }, [month]);

  const weeks = useMemo(() => {
    const weeksTemp: (Dayjs | null)[][] = [];
    const currentWeekYear = daysInMonth[0].week();
    const weekDayOfFirstMonthDay = +daysInMonth[0].day();
    let dayTemp = daysInMonth[0];

    while (dayTemp.isSameOrBefore(daysInMonth[daysInMonth.length - 1])) {
      const weekDayNum = +dayTemp.format('d');
      const weekOrderIndex = dayTemp.week() - currentWeekYear;
      if (!weeksTemp[weekOrderIndex]) weeksTemp[weekOrderIndex] = [];

      if (weekOrderIndex === 0 && weekDayOfFirstMonthDay > 0) {
        for (let i = 0; i < weekDayOfFirstMonthDay; i++) {
          weeksTemp[weekOrderIndex][i] = null;
        }
      }
      weeksTemp[weekOrderIndex][weekDayNum] = dayTemp;
      dayTemp = dayTemp.add(1, 'day');
    }

    return weeksTemp;
  }, [daysInMonth]);

  const getLongMonthName = useCallback(
    (monthInput: number) => {
      if (monthNames) {
        return monthNames[monthInput];
      }

      return (translateMonth || MONTHS)[monthInput];
    },
    [monthNames, translateMonth],
  );

  const getDayName = useCallback(
    (dayOfWeek: number, type: 'long' | 'short') => {
      if (type === 'long') {
        if (longDayNames) {
          return longDayNames[dayOfWeek];
        }

        return WEEK_DAYS_LONG[dayOfWeek];
      }
      if (shortDayNames) {
        return shortDayNames[dayOfWeek];
      }

      return (translateWeekday || WEEK_DAYS_SHORT)[dayOfWeek];
    },
    [longDayNames, shortDayNames, translateWeekday],
  );

  const updateMonth = useCallback(
    (isNext: boolean) => {
      let { month: monthTemp, year: yearTemp } = state;

      if (isNext) {
        yearTemp = monthTemp !== 11 ? yearTemp : yearTemp + 1;
        monthTemp = monthTemp !== 11 ? monthTemp + 1 : 0;
      } else {
        yearTemp = monthTemp !== 0 ? yearTemp : yearTemp - 1;
        monthTemp = monthTemp !== 0 ? monthTemp - 1 : 11;
      }

      setState({
        ...state,
        month: monthTemp,
        year: yearTemp,
      });
    },
    [month, year],
  );

  const dayHeader = useMemo(
    () =>
      DAY_HEADER.map((dayTemp, index) => (
        <th scope="col" key={index}>
          <abbr title={getDayName(dayTemp, 'long')}>{getDayName(dayTemp, 'short')}</abbr>
        </th>
      )),
    [getDayName],
  );

  const weekDayRender = weeks.map((week, index) => (
    <tr key={`${year}.${month}.week.${index}`}>
      {week.map((dayTemp, key) => {
        const isToday = dayTemp && dayTemp.isSame(today, 'date');
        const isActive = active && dayTemp && dayTemp.isSame(active, 'date');
        const isDisabled = dayTemp ? disableDates(dayTemp) : false;

        return (
          <td
            className={[
              'day',
              isActive ? 'active' : null,
              !dayTemp ? 'empty' : null,
              isToday ? 'today' : null,
              isDisabled && 'disabled',
            ]
              .filter((v) => v)
              .join(' ')}
            key={`${year}.${month}.day.${key}`}
            onClick={() => {
              if (!isDisabled) onDayClick(dayTemp);
            }}
          >
            <div className="content">{dayTemp ? dayTemp.format('DD') : ''}</div>
          </td>
        );
      })}
    </tr>
  ));

  return (
    <div className="react-daypicker-root">
      <div className="daypicker-header">
        <div className="previous-month" onClick={() => updateMonth(false)}>
          <LeftArrow fill="var(--xobk-default-text-secondary-color)" />
        </div>
        <div className="month-year">
          {getLongMonthName(month)} {year}
        </div>
        <div className="next-month" onClick={() => updateMonth(true)}>
          <RightArrow fill="var(--xobk-default-text-secondary-color)" />
        </div>
      </div>
      <table className="date-container">
        <thead>
          <tr>{dayHeader}</tr>
        </thead>
        <tbody>{weekDayRender}</tbody>
      </table>
    </div>
  );
};

export default Calendar;
