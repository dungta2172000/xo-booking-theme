import { Spinner } from '@xo/ui';
import './index.scss';

const Loading = () => (
  <div className="xobk-layout__body--loading">
    <Spinner size="large" />
  </div>
);

export default Loading;
