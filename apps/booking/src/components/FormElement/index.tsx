import Dropdown from '@components/Dropdown';
import { Checkbox, RadioButton, DatePicker, TextField, DropZone, Error } from '@xo/form';
import { t } from '@xo/utils2/src/translate';
import { IFormElementProps } from '../../../types/data/form';
import './formElement.scss';

export const FormElement = (props: IFormElementProps) => {
  const {
    element: { type, label, hideLabel, id, placeholder, helpText, validate, name },
    value,
    error,
    onBlur,
    onChange,
  } = props;

  if (type === 'text' || type === 'number' || type === 'email' || type === 'tel') {
    return (
      <div className="xobk-form__text-field">
        <TextField
          type={type}
          id={id}
          name={name}
          placeholder={t(placeholder)}
          helpText={t(helpText)}
          required={validate.required}
          value={value}
          label={t(label)}
          error={error}
          onChange={onChange}
          onBlur={onBlur}
          helpTextPosition="below"
        />
      </div>
    );
  }

  if (type === 'textarea') {
    return (
      <div className="xobk-form-textarea">
        <TextField
          type={type}
          id={id}
          name={name}
          placeholder={t(placeholder)}
          helpText={t(helpText)}
          required={validate.required}
          value={value}
          label={t(label)}
          error={error}
          onChange={onChange}
          onBlur={onBlur}
          helpTextPosition="below"
          multiline={true}
        />
      </div>
    );
  }

  if (type === 'radio') {
    return (
      <div className="xobk-form__group">
        <div className="xobk-form__radio">
          <span className="xobk-form__radio-label">{t(label)}</span>
          <div className="xobk-form__radios">
            {props.element.options?.map((option) => (
              <RadioButton
                onBlur={onBlur}
                key={option.id}
                id={option.id}
                name={id}
                onChange={onChange}
                label={t(option.label)}
                helpText={t(helpText)}
                value={t(option.label)}
              />
            ))}
          </div>
        </div>
        {t(helpText) && (
          <div className="xo-form-checkbox__help-text">
            <span>{t(helpText)}</span>
          </div>
        )}
        {error && <Error error={error} />}
      </div>
    );
  }

  if (type === 'checkbox') {
    return (
      <div className="xobk-form--checkbox">
        <Checkbox
          id={id}
          onChange={(valueCheckbox) => onChange(valueCheckbox)}
          onBlur={onBlur}
          error={error}
          checked={value[0]}
          labelHidden={hideLabel}
          text={t(label)}
          helpText={t(helpText)}
          value={value[0]}
        />
      </div>
    );
  }

  if (type === 'checkboxes') {
    return (
      <div className="xobk-form__group">
        <div className="xobk-form__radio">
          <span className="xobk-form__radio-label">{t(label)}</span>
          <div className="xobk-form__radios">
            {props.element.options?.map((option) => (
              <Checkbox
                onBlur={onBlur}
                key={option.id}
                id={option.id}
                checked={value.includes(t(option.label))}
                value={option.value}
                onChange={() => onChange(t(option.label))}
                text={t(option.label)}
                labelHidden={hideLabel}
              />
            ))}
          </div>
        </div>
        {t(helpText) && (
          <div className="xo-form-checkbox__help-text">
            <span>{t(helpText)}</span>
          </div>
        )}
        {error && <Error error={error} />}
      </div>
    );
  }

  if (type === 'file') {
    return (
      <DropZone
        accept={
          // eslint-disable-next-line no-nested-ternary
          validate.fileType === 'image'
            ? 'image/*'
            : validate.fileType === 'pdf'
            ? '.pdf'
            : '.zip, .rar'
        }
        multiple={props.element.allowMultiple}
        files={value}
        onDrop={onChange}
        onBlur={onBlur}
        label={t(label)}
        error={error}
        hint={t(helpText)}
        labelHidden={hideLabel}
        id={id}
      />
    );
  }

  if (type === 'date') {
    return (
      <div className="xobk-form-datePicker">
        <DatePicker
          id={id}
          label={t(label)}
          onChange={onChange}
          name={name}
          errorMessage={error}
          onBlur={onBlur}
          required={validate.required}
          value={value}
          helpText={t(helpText)}
        />
      </div>
    );
  }

  if (type === 'dropdown') {
    const options = props.element.options?.map((option) => ({
      label: t(option.label),
      value: t(option.label),
      id: option.id,
    }));

    return (
      <div className="xobk-form__dropdown">
        <Dropdown
          allowMultiple={props.element.allowMultiple ?? false}
          id={id}
          label={t(label)}
          onChange={onChange}
          name={name}
          error={error}
          required={validate.required}
          options={options as any}
          value={value}
          labelHidden={hideLabel}
          onBlur={onBlur}
          placeholder={t(placeholder)}
          helpText={t(helpText)}
        />
      </div>
    );
  }

  return null;
};
