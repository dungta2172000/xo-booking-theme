import { useContext } from 'preact/hooks';
import { AppContext } from '@store/context';
import { useSetting } from '@store/index';
import { t } from '@xo/utils2/src/translate';
import './header.scss';
import { TLabelNaming } from 'types/data/settings';

const Header = () => {
  const { useStore } = useContext(AppContext);
  const [setting] = useSetting();
  const [currentStep] = useStore.currentStep();

  const currentStepKey = Object.keys(setting.translation.progress)[
    currentStep - 1
  ] as keyof TLabelNaming['progress'];

  return (
    <div className="xobk-header">
      <div className="xobk-header__subtitle">
        {t(setting.translation.progress![currentStepKey].subTitle)}
      </div>
      <div className="xobk-header__title">
        {t(setting.translation.progress![currentStepKey].title)}
      </div>
    </div>
  );
};

export default Header;
