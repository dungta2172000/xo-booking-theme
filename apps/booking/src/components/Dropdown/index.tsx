import { Combobox, Select, TextField } from '@xo/form';
import { TChoice, TOptionsSelect } from '@xo/form/src/types/field';
import { useState } from 'preact/hooks';

interface IDropdown {
  id: string;
  name: string;
  allowMultiple: boolean;
  options: TOptionsSelect[];
  label: string;
  required?: boolean;
  placeholder?: string;
  labelHidden?: boolean;
  value: any;
  error?: string;
  onBlur?: () => void;
  onChange: (value: any) => void;
  helpText?: string;
}

const Dropdown = (props: IDropdown) => {
  const {
    id,
    allowMultiple,
    label,
    name,
    onChange,
    options,
    value,
    error,
    labelHidden,
    onBlur,
    placeholder,
    required,
    helpText,
  } = props;
  const [search, setSearch] = useState('');
  const [open, setOpen] = useState(false);

  if (allowMultiple) {
    return (
      <Combobox
        search={search}
        activator={
          <TextField
            type="text"
            value={search}
            label={label}
            onFocus={() => setOpen(true)}
            onChange={(val) => {
              setSearch(val);
            }}
          />
        }
        open={open}
        onClose={() => {
          if (onBlur) {
            onBlur();
          }
          setOpen(false);
        }}
        onSelect={(val) => {
          onChange(val);
        }}
        options={options as TChoice[]}
        value={value}
        allowMultiple={true}
        helpText={helpText}
      />
    );
  }

  return (
    <Select
      id={id}
      name={name}
      onChange={onChange}
      options={options ?? []}
      label={label}
      required={required}
      placeholder={placeholder}
      labelHidden={labelHidden}
      value={value}
      onBlur={onBlur || (() => {})}
      error={error}
      helpText={helpText}
    />
  );
};

export default Dropdown;
