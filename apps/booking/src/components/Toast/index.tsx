import { useEffect } from 'preact/hooks';
import { TToastType } from '../../../types/data/general';
import CloseIcon from '../../asset/CloseIcon';
import { useToast } from '../../store';
import styles from './toast.module.scss';

const Toast = () => {
  const [toast, setToast] = useToast();
  const { show, onClose, position, type, title, description } = toast;

  useEffect(() => {
    const interval = setInterval(() => {
      if (show && onClose) {
        onClose();
        setToast({ ...toast, show: false });
      }
    }, 4000);

    return () => {
      clearInterval(interval);
    };
  }, [show]);

  const closeToast = () => {
    if (onClose) onClose();
    setToast({ ...toast, show: false });
  };

  return (
    <div className={`${styles.notificationContainer} ${styles[position]}`}>
      {show && (
        <div
          className={`${styles.notification} ${styles.toast} ${styles[position]}  ${
            type === TToastType.success ? styles.toastSuccess : styles.toastError
          }`}
        >
          <button onClick={closeToast}>
            <CloseIcon color="#fff" />
          </button>
          <div>
            <p className={styles.notificationTitle}>{title}</p>
            <p className={styles.notificationMessage}>{description}</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Toast;
