/* eslint-disable no-restricted-syntax */
import { useCallback, useContext, useMemo } from 'preact/hooks';
import { Button } from '@xo/ui';
import { t } from '@xo/utils2/src/translate';
import { useOrderSubmit } from '@hooks/useOrderSubmit';
import { useFormSubmit } from '@hooks/useFormSubmit';
import { validate } from '@helper/validate';
import { getServiceInProductPage } from '@helper/service';
import {
  useButtonStatus,
  useDataSubmit,
  useFormErrors,
  useForms,
  useServices,
  useSetting,
  useToast,
} from '@store/index';
import { AppContext } from '@store/context';
import { DEFAULT_CUSTOMER_FORM } from '@constants/form';
import { TToastType } from 'types/data/general';
import { IForm } from 'types/data/form';
import './footer.scss';

const Footer = () => {
  const { useStore } = useContext(AppContext);
  const [currentStep, setCurrentStep] = useStore.currentStep();
  const [maxStep, setMaxStep] = useStore.maxStep();
  const [formData] = useForms();
  const [formError, setFormErrors] = useFormErrors();
  const [dataSubmit] = useDataSubmit();
  const [toast, setToast] = useToast();
  const submitForm = useFormSubmit();
  const submitOrder = useOrderSubmit();
  const [{ loading, disable }] = useButtonStatus();
  const [setting] = useSetting();
  const { button, toastNotification } = setting.translation;
  const [serviceList] = useServices();

  const isCustomer = typeof xoCustomerId !== 'undefined' && xoCustomerId !== '';

  // get position for scroll to top
  const bookingElement = document.getElementsByClassName('xo-booking')[0];
  const offset = 100;
  const elementPos = bookingElement.getBoundingClientRect().top;
  const offsetPos = elementPos + window.pageYOffset - offset;

  const validateForm = () => {
    const errors: Record<string, any> = {};

    // validate form
    for (const element of (formData as IForm).data.fields) {
      const error = validate(element, dataSubmit.form);
      if (error != null) {
        errors[element.id] = error;
      }
    }

    // validate customer info
    if (dataSubmit.service?.settings.skipCheckout && !isCustomer) {
      for (const element of DEFAULT_CUSTOMER_FORM) {
        const error = validate(element, dataSubmit.customer);
        if (error != null) {
          errors[element.id] = error;
        }
      }
    }

    setFormErrors(errors);
    return Object.keys(errors).length > 0;
  };

  const isDisableNext = useMemo(() => {
    if (currentStep < maxStep) return false;
    switch (currentStep) {
      case 1:
        return !dataSubmit?.variantId;
      case 2:
        return !dataSubmit?.timeStart;
      case 3:
        return Object.keys(formError).length > 0;
      default:
        return false;
    }
  }, [currentStep, maxStep, dataSubmit, formError]);

  const onSubmit = async () => {
    try {
      const formSubmitRes = await submitForm();
      const dataFormSubmitId = formSubmitRes.data.id as string;
      await submitOrder(dataFormSubmitId);
      if (dataSubmit.service?.settings.skipCheckout) {
        setToast({
          ...toast,
          show: true,
          title: t(toastNotification.bookingSuccess.title),
          description: t(toastNotification.bookingSuccess.description),
          type: TToastType.success,
        });
      }
    } catch (e: any) {
      setToast({
        ...toast,
        show: true,
        title: t(toastNotification.bookingFail.title),
        description: t(toastNotification.bookingFail.description),
        type: TToastType.failed,
      });
    }
  };

  const handleNextStep = useCallback(async () => {
    if (currentStep < 4) {
      window.scrollTo({
        top: offsetPos,
        behavior: 'smooth',
      });

      if (currentStep === 3) {
        const hasError = validateForm();

        if (hasError) {
          return;
        }
      }

      setCurrentStep(currentStep + 1);
      if (currentStep === maxStep) {
        setMaxStep(maxStep + 1);
      }
    } else {
      onSubmit();
    }
  }, [currentStep, maxStep, formData, dataSubmit]);

  const isDisableBackButton = () => {
    const serviceDetailInProductPage = getServiceInProductPage(serviceList);

    if (
      serviceDetailInProductPage &&
      currentStep === 2 &&
      serviceDetailInProductPage?.product?.variants?.length <= 1
    ) {
      return true;
    }
    return false;
  };

  return (
    <div className="xobk-footer">
      {currentStep !== 1 && (
        <div className="xobk-footer__back">
          <Button
            onClick={() => setCurrentStep(currentStep - 1)}
            disabled={currentStep === 1 || disable || loading || isDisableBackButton()}
          >
            {t(button.back)}
          </Button>
        </div>
      )}
      <div className="xobk-footer__confirm">
        <Button
          onClick={handleNextStep}
          primary
          disabled={isDisableNext || disable}
          loading={loading}
        >
          {currentStep === 4 ? t(button.confirm) : t(button.confirm)}
        </Button>
      </div>
    </div>
  );
};

export default Footer;
