import { FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import dayjs, { Dayjs } from 'dayjs';
import { Spinner } from '@xo/ui';
import { t } from '@xo/utils2/src/translate';
import { checkDuplicateAppointment, checkTimeOff, divideTimeSlot } from '@helper/appointment';
import { useDimension } from '@hooks/useDimension';
import { useDataSubmit, useSetting } from '@store/index';
import LeftArrow from '@asset/LeftArrow';
import RightArrow from '@asset/RightArrow';
import { IOrderedTime, ITimeOffSchedule, IWorkTimeRange, TWeekDay } from 'types/data/general';
import './timepicker.scss';

type TTimePickerProp = {
  selectedDate: Dayjs;
  schedule: IWorkTimeRange[];
  onSelectTime: (date: Dayjs) => void;
  disableDates: TWeekDay[];
};

const TimePicker: FunctionComponent<TTimePickerProp> = ({
  selectedDate,
  schedule,
  onSelectTime,
  disableDates,
}: TTimePickerProp) => {
  const isMobile = useDimension().width < 768;
  const [settings] = useSetting();

  const [timeSelected, setTimeSelected] = useState<Dayjs | null>(null);
  const [availableTime, setAvailableTime] = useState<Dayjs[]>([]);
  const [totalServiceTime, setTotalServiceTime] = useState<Dayjs[]>([]);
  const [loading, setLoading] = useState(false);
  const [{ service, timeStart }] = useDataSubmit();

  // Pagination for mobile version
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const currentHourList = isMobile
    ? totalServiceTime.slice((currentPage - 1) * 6, (currentPage - 1) * 6 + 6)
    : [];

  useEffect(() => {
    if (!timeSelected && timeStart) {
      setTimeSelected(dayjs(timeStart));
    }
  }, [timeStart]);

  useEffect(() => {
    // paging in mobile
    if (isMobile) {
      setTotalPage(Math.ceil(totalServiceTime.length / 6));
    }
  }, [totalServiceTime]);

  useEffect(() => {
    (async () => {
      setLoading(true);

      let timeCanNotBook = {
        serviceOrdered: [],
        timeOffSchedule: [],
      } as {
        serviceOrdered: IOrderedTime[];
        timeOffSchedule: ITimeOffSchedule[];
      };

      // check env for fetch service order vs time off schedule
      if (import.meta.env.VITE_NODE_ENV === 'development') {
        const serviceOrderedResponse = await fetch(
          `${import.meta.env.VITE_APP_ENDPOINT_PROD}/service-off.json`,
          {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
          },
        );

        timeCanNotBook = await serviceOrderedResponse.json();
      } else {
        const serviceOrderedResponse = await fetch(
          `${import.meta.env.VITE_APP_ENDPOINT_PROD}/service/service-available`,
          {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ date: selectedDate }),
          },
        );

        timeCanNotBook = await serviceOrderedResponse.json();
      }

      if (settings.bookingRule.allowDuplicateAppointment) {
        timeCanNotBook.serviceOrdered = [];
      }

      const { timeOffSchedule, serviceOrdered } = timeCanNotBook;

      const { availableTime: availableTimeTemp, totalServiceTime: totalServiceTimeTemp } =
        divideTimeSlot(
          selectedDate,
          settings.timeFormat.timezone,
          schedule,
          disableDates,
          service?.duration || 0,
          service?.prepareTime || { post: 0, pre: 0 },
        );

      const {
        availableTimeSlot: timeSlotAfterCheckAppointment,
        totalTimeSlot: finalTotalServiceTime,
      } = checkDuplicateAppointment(
        totalServiceTimeTemp,
        availableTimeTemp,
        serviceOrdered,
        service?.duration || 0,
        service?.prepareTime,
      );

      const timeSlotFinal = checkTimeOff(
        timeSlotAfterCheckAppointment,
        timeOffSchedule,
        service?.duration || 0,
      );

      setAvailableTime(timeSlotFinal);
      setTotalServiceTime(finalTotalServiceTime);

      setLoading(false);
    })();
  }, [selectedDate, schedule, disableDates]);

  const handleSelectTime = (time: Dayjs) => {
    setTimeSelected(time);
    onSelectTime(time);
  };

  return (
    <>
      {isMobile && (
        <div className="xobk-timepicker-pagination-buttons">
          <button
            className="xobk-timepicker-pagination-button"
            onClick={() => {
              if (currentPage > 1) {
                setCurrentPage(currentPage - 1);
              }
            }}
          >
            <LeftArrow fill={currentPage === 1 ? '#E4E4E4' : '#727272'} />
          </button>
          <button
            className="xobk-timepicker-pagination-button"
            onClick={() => {
              if (currentPage < totalPage) {
                setCurrentPage(currentPage + 1);
              }
            }}
          >
            <RightArrow fill={currentPage === totalPage ? '#E4E4E4' : '#727272'} />
          </button>
        </div>
      )}

      <div className={loading ? 'xobk-timepicker-loading' : 'xobk-timepicker'}>
        {loading ? (
          <Spinner />
        ) : (
          <>
            {totalServiceTime.length === 0 || availableTime.length === 0 || !selectedDate ? (
              <div className="xobk-timePicker__day-not-available">
                {t(settings.translation.dayNotAvailable) ?? 'Time is not available'}
              </div>
            ) : (
              <>
                {(isMobile ? currentHourList : totalServiceTime).map((time: Dayjs) => {
                  const isDisable = !availableTime.some((availableTimeTemp) =>
                    availableTimeTemp.isSame(time),
                  );

                  const timeFormatString =
                    settings.timeFormat.hourFormat === 'am/pm' ? 'hh:mm A' : 'HH:mm';

                  const timeValue = time.format(timeFormatString);
                  return (
                    <div
                      key={time}
                      onClick={() => {
                        if (!isDisable) handleSelectTime(time);
                      }}
                      className="xobk-timepicker-hour"
                    >
                      <input
                        type="radio"
                        name="hour"
                        id={timeValue}
                        checked={!!timeSelected && time.isSame(timeSelected)}
                        className="xobk-timepicker-radio"
                      />
                      <label
                        htmlFor={timeValue}
                        className={`xobk-timepicker-label${
                          isDisable ? ' xobk-timepicker-hour__not-available' : ''
                        }`}
                      >
                        {timeValue}
                      </label>
                    </div>
                  );
                })}
              </>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default TimePicker;
