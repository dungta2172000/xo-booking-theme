import { Fragment } from 'preact';
import { useContext } from 'preact/hooks';
import { AppContext } from '@store/context';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';

const StepDetail = () => {
  const { useStore } = useContext(AppContext);
  const [currentStep] = useStore.currentStep();

  switch (currentStep) {
    case 1:
      return <Step1 />;
    case 2:
      return <Step2 />;
    case 3:
      return <Step3 />;
    case 4:
      return <Step4 />;
    default:
      return <Fragment />;
  }
};

export default StepDetail;
