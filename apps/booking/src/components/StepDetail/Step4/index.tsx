/* eslint-disable no-nested-ternary */
import { useContext } from 'preact/hooks';
import dayjs from 'dayjs';
import { t } from '@xo/utils2/src/translate';
import { useDataSubmit, useForms, useSetting } from '@store/index';
import { AppContext } from '@store/context';
import { DEFAULT_CUSTOMER_FORM } from '@constants/form';
import { IElementOptions, IFormELementData } from 'types/data/form';
import './step4.scss';

const Step4 = () => {
  const { useStore } = useContext(AppContext);
  const [, setCurrentStep] = useStore.currentStep();
  const [{ timeStart, service, form, customer }] = useDataSubmit();
  const [settings] = useSetting();
  const [formFormat] = useForms();
  const {
    schedule: scheduleTranslation,
    customer: customerTranslation,
    changeButton: changeButtonLabel,
  } = settings.translation.confirmInfo;

  const isCustomer = typeof xoCustomerId !== 'undefined' && xoCustomerId !== '';

  const getFormElementData = (data: [string, unknown]) => {
    const id = data[0];
    const value: any = data[1];

    const currentForm = service?.settings.skipCheckout
      ? !isCustomer
        ? [...DEFAULT_CUSTOMER_FORM, ...formFormat.data.fields]
        : formFormat.data.fields
      : formFormat.data.fields;

    const element = currentForm.find((el: IFormELementData) => el.id === id) as IFormELementData;

    let label = '';
    if (service?.settings?.skipCheckout && ['firstName', 'lastName', 'email'].includes(id)) {
      label = t(
        settings.translation.skipCheckoutForm[
          id as keyof typeof settings.translation.skipCheckoutForm
        ],
      );
    } else {
      label = t(element.label) ?? '';
    }

    if (Array.isArray(value) && value.length && value[0] instanceof File) {
      return {
        label,
        value: value[0].name,
      };
    }

    if (Array.isArray(value)) {
      if (element.options) {
        const valueLabel = value.map((val) => {
          // @ts-ignore
          const el = element?.options.find((option: IElementOptions) => option.value === val);
          if (el) {
            return t(el.label);
          }
          return val;
        });

        return {
          label,
          value: valueLabel.join(', '),
        };
      }
      return {
        label,
        value: typeof value[0] === 'boolean' ? (value[0] ? 'Yes' : 'No') : value[0],
      };
    }

    return {
      label,
      value,
    };
  };

  const timeFormatString = settings.timeFormat.hourFormat === 'am/pm' ? 'hh:mma' : 'HH:mm';

  const startTimeString = dayjs(timeStart).format(timeFormatString);
  const endTimeString = dayjs(timeStart)
    .add(service?.duration || 0, 'minute')
    .format(timeFormatString);

  return (
    <div className="xobk-step4">
      <div className="xobk-step4-header">
        <div className="xobk-step4-title">{t(scheduleTranslation)}</div>
        <button className="xobk-step4-header-btn" onClick={() => setCurrentStep(2)}>
          {'>'} {t(changeButtonLabel)}
        </button>
      </div>
      <div className="xobk-step4-schedule">{`${startTimeString}-${endTimeString} - ${new Date(
        timeStart as Date,
      ).toLocaleDateString(Shopify.locale, {
        month: 'long',
        day: '2-digit',
        year: 'numeric',
      })}`}</div>
      <div className="xobk-step4-header">
        <div className="xobk-step4-title">{t(customerTranslation)}</div>
        <button className="xobk-step4-header-btn" onClick={() => setCurrentStep(3)}>
          {'>'} {t(customerTranslation)}
        </button>
      </div>
      <div className="xobk-step4-form">
        {service?.settings.skipCheckout &&
          !isCustomer &&
          Object.entries(customer).map((el, index) => {
            const data = getFormElementData(el);
            return (
              <div className="xobk-step4-form__element" key={index}>
                <div className="xobk-step4-form__element-label">{data.label}</div>
                <div className="xobk-step4-form__element-value">{data.value || '[No value]'}</div>
              </div>
            );
          })}
        {Object.entries(form).map((el, index) => {
          const data = getFormElementData(el);
          return (
            <div className="xobk-step4-form__element" key={index}>
              <div className="xobk-step4-form__element-label">{data.label}</div>
              <div className="xobk-step4-form__element-value">{data.value || '[No value]'}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Step4;
