import { useCallback, useEffect, useState } from 'preact/hooks';
import { useFetch } from '@xo/utils2';
import { FormElement } from '@components/FormElement';
import Loading from '@components/Loading';
import { useSetLoadingState } from '@hooks/useSetLoadingState';
import { validate } from '@helper/validate';
import { useDataSubmit, useFormErrors, useForms, useSetting } from '@store/index';
import { postProxyUrl } from '@constants/url';
import { useDimension } from '@hooks/useDimension';
import { DEFAULT_CUSTOMER_FORM } from '@constants/form';
import { IForm, IFormELementData } from 'types/data/form';
import './step3.scss';

const Step3 = () => {
  const [serviceSubmit] = useDataSubmit.service();

  const { data, state } = useFetch(
    postProxyUrl(
      `form${
        import.meta.env.VITE_NODE_ENV === 'production' ? `/${serviceSubmit?.customFormId}` : ''
      }`,
    ),
    useForms,
  );
  useSetLoadingState(state.status);

  const isMobile = useDimension().width < 768;
  const [formValue, setFormValue] = useDataSubmit.form();
  const [customerInfo, setCustomerInfo] = useDataSubmit.customer();
  const [serviceChoose] = useDataSubmit.service();
  const [formError, setFormError] = useFormErrors();
  const [loading, setLoading] = useState(true);
  const isCustomer = typeof xoCustomerId !== 'undefined' && xoCustomerId !== '';
  const [setting] = useSetting();
  const { skipCheckoutForm } = setting.translation;

  const isBlockElement = (type: string) => {
    if (type === 'checkbox' || type === 'radio' || type === 'checkboxes' || type === 'file') {
      return true;
    }

    return false;
  };

  useEffect(() => {
    if (skipCheckoutForm?.emailHelperText) {
      // @ts-ignore
      DEFAULT_CUSTOMER_FORM[2].helpText = skipCheckoutForm.emailHelperText;
    }
    if (skipCheckoutForm?.emailRequiredText) {
      // @ts-ignore
      DEFAULT_CUSTOMER_FORM[2].validate.requiredText = skipCheckoutForm.emailRequiredText;
    }
  }, []);

  useEffect(() => {
    if (data && Object.keys(data).length) {
      const newFormValue = { ...formValue };
      (data as IForm).data.fields.forEach((item: IFormELementData) => {
        if (!formValue[item.id]) {
          if (item.type === 'dropdown' || item.type === 'file') {
            if (item.allowMultiple) {
              newFormValue[item.id] = item.defaultValue ?? [];
            } else {
              newFormValue[item.id] = item.defaultValue ? [item.defaultValue] : [];
            }
          } else if (item.type === 'checkboxes') {
            newFormValue[item.id] = item.defaultValue ?? [];
          } else if (item.defaultValue) {
            newFormValue[item.id] = item.defaultValue;
          } else {
            newFormValue[item.id] = '';
          }
        }
      });
      setFormValue(newFormValue);
      setLoading(false);
    }
  }, [data, state.status]);

  const handleDeleteFieldError = useCallback(
    (fieldId: string) => {
      const newError = { ...formError };
      delete newError[fieldId];
      setFormError(newError);
    },
    [formError],
  );

  const onChange = (type: string, name: string, value: any) => {
    const newValue = { ...formValue } ?? {};
    if (type === 'radio' || type === 'checkbox') {
      newValue[name] = [value];
    } else if (type === 'dropdown') {
      newValue[name] = value;
    } else if (type === 'checkboxes') {
      if (formValue[name] && formValue[name].includes(value)) {
        newValue[name] = newValue[name].filter((item: string | number) => item !== value);
      } else if (formValue[name]) {
        newValue[name].push(value);
      } else {
        newValue[name] = [value];
      }
    } else {
      newValue[name] = value;
    }

    setFormValue({ ...formValue, ...newValue });
    handleDeleteFieldError(name);
  };

  const handleBlur = (item: IFormELementData, dataSaveForm: 'form' | ' customer') => {
    const formValueInput = dataSaveForm === 'form' ? formValue : customerInfo;
    const error = validate(item, formValueInput);

    if (error != null) {
      setFormError({ ...formError, [item.id]: error });
    } else if (formError[item.id] || formError[item.id] === '') {
      handleDeleteFieldError(item.id);
    }
  };

  if (loading) {
    return <Loading />;
  }

  console.log(formError);

  return (
    <div className="xobk-step3">
      {serviceChoose?.settings.skipCheckout && !isCustomer && (
        <div className={'xobk-form--section'}>
          {DEFAULT_CUSTOMER_FORM.map((customerFormElem) => (
            <FormElement
              key={customerFormElem.id}
              onBlur={() => handleBlur(customerFormElem, ' customer')}
              element={{
                ...customerFormElem,
                // @ts-ignore
                ...(skipCheckoutForm[customerFormElem.id]
                  ? // @ts-ignore
                    { label: skipCheckoutForm[customerFormElem.id] }
                  : {}),
              }}
              error={formError[customerFormElem.id]}
              onChange={(value) =>
                setCustomerInfo({ ...customerInfo, [customerFormElem.id]: value })
              }
              // @ts-ignore
              value={customerInfo[`${customerFormElem.id}`]}
              // @ts-ignore
              defaultValue={customerInfo[`${customerFormElem.id}`]}
            />
          ))}
        </div>
      )}
      {(data as IForm).data.fields.map((item: IFormELementData) => (
        <div
          key={item.id}
          className={
            isMobile || isBlockElement(item.type) ? 'xobk-step3-blockElement' : 'xobk-step3-element'
          }
        >
          <FormElement
            onBlur={() => handleBlur(item, 'form')}
            element={item}
            error={formError[item.id]}
            onChange={(value) => onChange(item.type, item.id, value)}
            value={formValue && formValue[item.id]}
            defaultValue={item.defaultValue}
          />
        </div>
      ))}
    </div>
  );
};

export default Step3;
