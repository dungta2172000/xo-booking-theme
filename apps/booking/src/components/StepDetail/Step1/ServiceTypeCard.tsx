import { FunctionalComponent } from 'preact';
import clsx from 'clsx';
import { useSetting } from '@store/index';
import { RadioButton } from '@xo/form';
import { formatMoney } from '@xo/utils2/src/formatMoney';
import { IService } from 'types/data/service';
import { getServiceTimeUnit } from '@helper/string';
import { useServiceHelper } from './useServiceHelper';
import './index.scss';

type TServiceCardProps = {
  service: IService;
};

const ServiceTypeCard: FunctionalComponent<TServiceCardProps> = ({
  service,
}: TServiceCardProps) => {
  const [setting] = useSetting();
  const { time } = setting.translation.calendar;

  const { title, duration, _id: serviceId, product } = service;
  const { handleCheckVariant, description, thumbnail, serviceIdCheck, variantCheck } =
    useServiceHelper(service);
  const { content: descriptionContent, isAllowDescription } = description;
  const { isAllowThumbnail, url: thumbnailUrl } = thumbnail;

  return (
    <div
      className={clsx(
        'xobk-service',
        'xobk-service--multivariant',
        !isAllowThumbnail && 'xobk-service-no-thumb',
      )}
    >
      <div className="xobk-service__body">
        <div className="xobk-service__checkbox">
          <RadioButton
            label=""
            onChange={() => {}}
            checked={serviceIdCheck === serviceId}
            value={serviceId}
            name="service"
            id={serviceId}
          />
        </div>
        {isAllowThumbnail && (
          <img
            className="xobk-service__image"
            alt="title"
            src={thumbnailUrl}
            // onError={(e: any) => (e.target.src = defaultImgUrl)}
          />
        )}
        <div className="xobk-service__detail">
          <h1 className="xobk-service__title">{title}</h1>
          {isAllowDescription && descriptionContent && (
            // eslint-disable-next-line react/no-danger
            <div
              className="xobk-service__description"
              dangerouslySetInnerHTML={{ __html: descriptionContent }}
            />
          )}
          {variantCheck && (
            <div className="xobk-service-variant--selected">
              <div className="xobk-service-variant__name">{variantCheck.title}</div>
              <div
                className="xobk-service-variant__price"
                dangerouslySetInnerHTML={{
                  __html: `${formatMoney(
                    +variantCheck.price * 100,
                  )} / ${duration} ${getServiceTimeUnit(duration, time)}`,
                }}
              />
            </div>
          )}
        </div>
      </div>

      <div className="xobk-service__body--back">
        <h1 className="xobk-service__title">{title}</h1>
        <div className="xobk-service__variants">
          {product.variants.map(({ title: variantTitle, price, id }) => (
            <div className="xobk-service__variant" key={id}>
              <div>
                <RadioButton
                  label=""
                  onChange={() => {
                    handleCheckVariant(id);
                  }}
                  checked={id.toString() === variantCheck?.id?.toString()}
                  value={id.toString()}
                  name="service-variant"
                  id={id.toString()}
                />
              </div>
              <label htmlFor={id.toString()} className="xobk-service-variant__detail">
                <div className="xobk-service-variant__name">
                  {variantTitle.toLocaleLowerCase() === 'default title' ? title : variantTitle}
                </div>
                <div
                  className="xobk-service-variant__price"
                  dangerouslySetInnerHTML={{
                    __html: `${formatMoney(+price * 100)} / ${duration} ${getServiceTimeUnit(
                      duration,
                      time,
                    )}`,
                  }}
                />
              </label>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ServiceTypeCard;
