import { DEFAULT_SERVICE_THUMBNAIL } from '@constants/url';
import { useDataSubmit, useForms, useSetting } from '@store/index';
import { useState } from 'preact/hooks';
import { IService } from 'types/data/service';

export const useServiceHelper = (service: IService) => {
  const [dataSubmit, setDataSubmit] = useDataSubmit();
  const [settings] = useSetting();
  const [descriptionOpen, setDescriptionOpen] = useState<string[]>([]);
  const [, setForm] = useForms();

  const { _id: serviceId, product, isCustomProduct, description, media } = service;
  const { variant: appearanceVariant } = settings.storeLayout.appearance;

  const isShowDescription = () => {
    if ([5, 6].includes(appearanceVariant)) return true;
    if ([2, 3].includes(appearanceVariant) && descriptionOpen.includes(serviceId)) return true;
    return false;
  };

  const isAllowDescription = [2, 3, 5, 6].includes(appearanceVariant);
  const isAllowThumbnail = [3, 4, 6].includes(appearanceVariant);

  const handleCheckVariant = (variantId: number) => {
    //check current variant select, if change => set form again
    if (variantId !== dataSubmit.variantId) {
      setForm({} as any);
    }
    setDataSubmit({
      ...dataSubmit,
      variantId,
      service,
      ...(variantId !== dataSubmit.variantId ? { form: {} } : {}),
    });
  };

  const toggleDescriptionOpen = () => {
    const idIndex = descriptionOpen.indexOf(serviceId);
    if (idIndex <= -1) setDescriptionOpen([...descriptionOpen, serviceId]);
    else {
      const newService = descriptionOpen.filter((serviceIdTemp) => serviceIdTemp !== serviceId);
      setDescriptionOpen(newService);
    }
  };

  const getThumbnailUrl = (): string => {
    if (isCustomProduct && media.length) return media[0].src;
    if (product.image) return product.image.src;
    return DEFAULT_SERVICE_THUMBNAIL;
  };

  const variantCheck = service.product.variants.find(
    (variantTemp) => variantTemp.id === dataSubmit.variantId,
  );

  return {
    handleCheckVariant,
    variantCheck,
    serviceIdCheck: dataSubmit.service?._id,
    description: {
      content: isCustomProduct && description ? description : product.description,
      isShowDescription: isShowDescription(),
      toggleDescriptionOpen,
      isAllowDescription,
    },
    thumbnail: { isAllowThumbnail, url: getThumbnailUrl() },
  };
};
