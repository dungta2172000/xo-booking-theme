/* eslint-disable no-underscore-dangle */
import { FunctionComponent } from 'preact';
import { RadioButton } from '@xo/form';
import { Collapsible } from '@xo/ui';
import { formatMoney } from '@xo/utils2/src/formatMoney';
import { useSetting } from '@store/index';
import UpDownArrow from '@asset/UpDownArrow';
import { useDimension } from '@hooks/useDimension';
import { getServiceTimeUnit } from '@helper/string';
import { DEFAULT_SERVICE_THUMBNAIL } from '@constants/url';
import { IService } from 'types/data/service';
import { useServiceHelper } from './useServiceHelper';
import './index.scss';

export type TServiceItem = {
  service: IService;
};

const ServiceTypeList: FunctionComponent<TServiceItem> = ({ service }: TServiceItem) => {
  const isMobile = useDimension().width < 768;
  const { title, duration, product } = service;
  const [setting] = useSetting();
  const { time } = setting.translation.calendar; // Service time unit
  const { handleCheckVariant, variantCheck, description, thumbnail } = useServiceHelper(service);
  const {
    content: descriptionContent,
    isShowDescription,
    toggleDescriptionOpen,
    isAllowDescription,
  } = description;
  const { isAllowThumbnail, url: thumbnailUrl } = thumbnail;

  return (
    <div className="xobk-service">
      {isAllowThumbnail && (
        <img
          src={thumbnailUrl}
          className="xobk-service__image"
          // eslint-disable-next-line no-return-assign
          onError={(e: any) => (e.target.src = DEFAULT_SERVICE_THUMBNAIL)}
        />
      )}

      <div className="xobk-service__content">
        <div className="xobk-service__heading">
          {product.variants.length === 1 ? (
            <div className="xobk-service__variant xobk-service__title-main--checkable">
              <RadioButton
                // @ts-ignore
                label={
                  <>
                    {title}
                    <span
                      dangerouslySetInnerHTML={{
                        __html: `${!isMobile ? ' - ' : ''} ${formatMoney(
                          +product.variants[0].price * 100,
                        )} / ${duration} ${getServiceTimeUnit(duration, time)}`,
                      }}
                    ></span>
                  </>
                }
                onChange={() => {
                  handleCheckVariant(product.variants[0].id);
                }}
                checked={product.variants[0].id.toString() === variantCheck?.id?.toString()}
                value={product.variants[0].id.toString()}
                name="service"
                id={product.variants[0].id.toString()}
              />
            </div>
          ) : (
            <div className="xobk-service__title-main">{title}</div>
          )}

          {isAllowDescription && descriptionContent && (
            <div
              className={'xobk-service__description'}
              style={{ width: isAllowThumbnail ? '100%' : '90%' }}
              data-state={isShowDescription ? 'open' : 'close'}
            >
              <Collapsible isOpen={isShowDescription}>
                {/* eslint-disable-next-line react/no-danger */}
                <div dangerouslySetInnerHTML={{ __html: descriptionContent }} />
              </Collapsible>
            </div>
          )}
        </div>
        {product.variants.length > 1 &&
          product.variants[0].title.toLocaleLowerCase() !== 'default title' && (
            <div className="xobk-service__variants">
              {product.variants.map(({ title: variantTitle, price, id }) => (
                <div
                  className={`xobk-service__variant ${
                    isMobile ? 'xobk-service__variant--mobile' : ''
                  }`}
                  key={id}
                >
                  <RadioButton
                    // @ts-ignore
                    label={
                      <>
                        {variantTitle}
                        <span
                          dangerouslySetInnerHTML={{
                            __html: `${!isMobile ? ' - ' : ''}${formatMoney(
                              +price * 100,
                            )} / ${duration} ${getServiceTimeUnit(duration, time)}`,
                          }}
                        />
                      </>
                    }
                    onChange={() => {
                      handleCheckVariant(id);
                    }}
                    checked={id.toString() === variantCheck?.id?.toString()}
                    value={id.toString()}
                    name="service"
                    id={id.toString()}
                  />
                </div>
              ))}
            </div>
          )}
      </div>
      {isAllowDescription && descriptionContent && (
        <button onClick={toggleDescriptionOpen} className="xobk-service__toggle-description">
          <UpDownArrow state={isShowDescription ? 'down' : 'up'} />
        </button>
      )}
    </div>
  );
};

export default ServiceTypeList;
