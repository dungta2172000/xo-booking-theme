import { useCallback, useContext, useEffect, useState } from 'preact/hooks';
import clsx from 'clsx';
import { useFetch } from '@xo/utils2';
import { AppContext } from '@store/context';
import { useDataSubmit, useServices, useSetting } from '@store/index';
import Loading from '@components/Loading';
import ServiceTypeList from './ServiceTypeList';
import ServiceTypeCard from './ServiceTypeCard';
import { useSetLoadingState } from '@hooks/useSetLoadingState';
import { useDimension } from '@hooks/useDimension';
import { getServiceInProductPage } from '@helper/service';
import { postProxyUrl } from '@constants/url';
import { IService } from 'types/data/service';
import './index.scss';

const Step1 = () => {
  const { useStore } = useContext(AppContext);
  const { data, state } = useFetch(postProxyUrl('services'), useServices);
  const [dataSubmit, setDataSubmit] = useDataSubmit();
  const [currentStep, setCurrentStep] = useStore.currentStep();
  const [maxStep, setMaxStep] = useStore.maxStep();
  useSetLoadingState(state.status);
  const [setting] = useSetting();
  const isMobile = useDimension().width < 768;
  const { variant: serviceDisplayVariant, cardPerRow } = setting.storeLayout.appearance;

  const serviceTypeRender = [1, 2, 3].includes(serviceDisplayVariant) ? 'list' : 'card';
  const [serviceListTemp, setServiceListTemp] = useState<IService[]>([]);

  useEffect(() => {
    if (data && data?.length) {
      const serviceDetailProductPage = getServiceInProductPage(data);
      if (serviceDetailProductPage) {
        setServiceListTemp([serviceDetailProductPage]);
        if (serviceDetailProductPage.product.variants.length <= 1) {
          // go to next step and service choose is service in product page
          setDataSubmit({
            ...dataSubmit,
            service: serviceDetailProductPage,
            variantId: serviceDetailProductPage.product.variants[0].id,
          });
          setCurrentStep(currentStep + 1);
          setMaxStep(maxStep + 1);
        }
      } else {
        setServiceListTemp(data);
      }
    }
  }, [data]);

  const getServiceRender = useCallback(
    (service: IService) => {
      if ([1, 2, 3].includes(serviceDisplayVariant)) return <ServiceTypeList service={service} />;
      if ([4, 5, 6].includes(serviceDisplayVariant)) return <ServiceTypeCard service={service} />;
      return <></>;
    },
    [serviceDisplayVariant],
  );

  if (state.status === 'loading') {
    return <Loading />;
  }

  return (
    <div className={`xobk-services--layout-${serviceTypeRender}`}>
      <div
        className={clsx('xobk-services', serviceDisplayVariant > 3 && 'xobk-services-grid')}
        style={
          !isMobile && Number(serviceDisplayVariant > 3)
            ? {
                gridTemplateColumns: `repeat(${cardPerRow || 1}, calc(${
                  100 / cardPerRow || 1
                }% - 12px))`,
              }
            : {}
        }
      >
        {serviceListTemp!.map(getServiceRender)}
      </div>
    </div>
  );
};

export default Step1;
