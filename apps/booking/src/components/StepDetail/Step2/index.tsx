import { useEffect, useState } from 'preact/hooks';
import dayjs, { Dayjs } from 'dayjs';
import { useFetch } from '@xo/utils2';
import Calendar from '@components/Calendar';
import Loading from '@components/Loading';
import TimePicker from '@components/TimePicker';
import { useSetLoadingState } from '@hooks/useSetLoadingState';
import { getDisableServiceDay } from '@helper/appointment';
import { useDataSubmit, useServices, useSetting } from '@store/index';
import { postProxyUrl } from '@constants/url';
import { IWorkTimeRange, TWeekDay } from 'types/data/general';
import { useDimension } from '@hooks/useDimension';
import { IService } from 'types/data/service';
import './step2.scss';

const Step2 = () => {
  const [dataSubmit, setDataSubmit] = useDataSubmit();
  const isMobile = useDimension().width < 768;
  const { service, timeStart } = dataSubmit;
  const [selectedDate, setSelectedDate] = useState<Dayjs | null>(
    timeStart
      ? dayjs(timeStart as Date)
          .set('hour', 0)
          .set('minute', 0)
          .set('second', 0)
      : dayjs().set('hour', 0).set('minute', 0).set('second', 0),
  );
  const { data: expertWorkTime, state } = useFetch(
    postProxyUrl(
      import.meta.env.VITE_NODE_ENV === 'development' ? 'schedule' : 'expert/1/available',
    ),
  );
  const [services] = useServices();
  const [settings] = useSetting();
  useSetLoadingState(state.status);

  const [offDays, setOffDays] = useState<TWeekDay[]>([]);

  const currentServiceDetail = services.find(
    (serviceTemp) => serviceTemp._id === service?._id,
  ) as IService;
  const schedule = (
    currentServiceDetail?.workTime?.isHasDetail
      ? currentServiceDetail.workTime.detail
      : expertWorkTime?.expertWorkTime
  ) as IWorkTimeRange[];

  const setTimePickerHeight = () => {
    const calendarHeight = document.getElementById('xobk-calendar')?.clientHeight;
    document
      .getElementById('xobk-timepicker')
      ?.setAttribute('style', `height: ${calendarHeight}px;`);
  };

  // setting timezone when get from json is converted to string
  useEffect(() => {
    if (state.status === 'fetched') {
      const disableDaysTemp = getDisableServiceDay(
        currentServiceDetail,
        expertWorkTime?.expertWorkTime as IWorkTimeRange[],
        settings.timeFormat.timezone,
      );

      const resetActiveDay = () => {
        let targetDay = timeStart
          ? (selectedDate as Dayjs)
          : (selectedDate as Dayjs).add(+settings.bookingRule.bookBefore, 'day');
        let targetWeekDay = targetDay.format('ddd') as TWeekDay;

        if (disableDaysTemp.length === 7) setSelectedDate(null);
        else {
          while (disableDaysTemp.includes(targetWeekDay)) {
            targetDay = targetDay.add(1, 'day');
            targetWeekDay = targetDay.format('ddd') as TWeekDay;
          }

          setSelectedDate(targetDay);
        }
      };

      resetActiveDay();
      setOffDays(disableDaysTemp);

      if (!isMobile) {
        setTimePickerHeight();
      }
    }
  }, [state]);

  if (state.status === 'loading') {
    return <Loading />;
  }

  return (
    <div className="xobk-step2-container">
      <div className="xobk-step2-calendar" id="xobk-calendar">
        <Calendar
          active={selectedDate}
          onDayClick={(date) => setSelectedDate(date)}
          disableDates={(date: Dayjs) =>
            date.isBefore(dayjs().add(+settings.bookingRule.bookBefore || 0, 'day'), 'date') ||
            offDays.includes(date.format('ddd') as TWeekDay)
          }
        />
      </div>
      <div className="xobk-step2-timepicker" id="xobk-timepicker">
        <TimePicker
          disableDates={offDays}
          schedule={schedule || []}
          selectedDate={selectedDate!}
          onSelectTime={(selectedDateTemp) =>
            setDataSubmit({ ...dataSubmit, timeStart: selectedDateTemp.toDate() })
          }
        />
      </div>
    </div>
  );
};

export default Step2;
