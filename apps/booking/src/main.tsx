import { render } from 'preact';
import { Spinner } from '@xo/ui';
import { useFetch } from '@xo/utils2';
import { genId } from '@xo/utils2/src/genId';
import { AppProvider } from '@store/context';
import { ISettings } from 'types/data/settings';
import { postProxyUrl } from './constants/url';
import Layout from './Layout';
import { useSetting } from './store';
import '@xo/form/src/styles/form.scss';
import './styles/app.scss';

const App = () => {
  const { data: setting, state } = useFetch(postProxyUrl('settings'), useSetting);
  const uniqueId = genId('_');

  if (state.status === 'loading' && !Object.keys(setting as ISettings).length) {
    return <Spinner />;
  }

  return (
    <AppProvider id={uniqueId}>
      <div
        className="xobk-wrapper"
        style={`--xo-bk-primary: ${setting!.storeLayout.colorTheme};
                --xo-bk-primary-1: ${setting!.storeLayout.colorTheme}B3;
                --xo-bk-primary-2: ${setting!.storeLayout.colorTheme}80;
                --xo-bk-primary-3: ${setting!.storeLayout.colorTheme}33;
      `}
      >
        <Layout />
      </div>
    </AppProvider>
  );
};

const AppElements = document.querySelectorAll<HTMLElement>('.xo-booking');
for (let i = 0; i < AppElements.length; i += 1) {
  const element = AppElements[i];

  render(<App />, element);
}
