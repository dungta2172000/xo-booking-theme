/* eslint-disable max-len */
type TProps = {
  state: 'up' | 'down';
};

const UpDownArrow = ({ state }: TProps) => (state === 'down' ? (
    <svg width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M0.188343 8.50871C-0.0776959 8.20434 -0.0597237 7.72985 0.228485 7.44889L6.86514 0.979314L13.5018 7.44889C13.79 7.72985 13.808 8.20434 13.5419 8.50871C13.2759 8.81307 12.8266 8.83205 12.5384 8.5511L6.86514 3.02068L1.1919 8.5511C0.903687 8.83205 0.454381 8.81307 0.188343 8.50871Z"
        fill="#727272"
      />
    </svg>
) : (
    <svg width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M13.5421 1.22176C13.8082 1.52613 13.7902 2.00062 13.502 2.28158L6.86533 8.75115L0.228673 2.28157C-0.059536 2.00062 -0.077507 1.52613 0.188531 1.22176C0.454569 0.917396 0.903875 0.898417 1.19208 1.17937L6.86533 6.70979L12.5386 1.17937C12.8268 0.898417 13.2761 0.917397 13.5421 1.22176Z"
        fill="#727272"
      />
    </svg>
));

export default UpDownArrow;
