const RightArrow = ({ fill }: { fill: string }) => (
  <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M1.16951 10.8926C0.910411 10.6335 0.910411 10.2134 1.16951 9.95431L5.12345 6.00038L1.16951 2.04644C0.91041 1.78734 0.91041 1.36726 1.16951 1.10816C1.42861 0.849063 1.84869 0.849063 2.10778 1.10816L7 6.00038L2.10779 10.8926C1.84869 11.1517 1.42861 11.1517 1.16951 10.8926Z"
      fill={fill}
    />
  </svg>
);

export default RightArrow;
