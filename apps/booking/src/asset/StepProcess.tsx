const StepProcess = ({
  step,
  thumbColor,
  progressBarColor,
}: {
  step: number;
  thumbColor?: string;
  progressBarColor?: string;
}) => {
  const thumbColorTemp = thumbColor ?? '#45887C';
  const progressBarColorTemp = progressBarColor ?? '#6BB1A5';

  if (step === 1) {
    return (
      <svg width="100%" viewBox="0 0 287 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="10" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="6" cy="6" r="5" stroke={thumbColorTemp} stroke-width="2" />
        <rect x="12.5" y="5.5" width="78.6667" height="1" stroke="#D3D3D3" />
        <rect x="92.1665" y="5.5" width="1" height="1" stroke="#D3D3D3" />
        <rect x="101.667" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="97.6665" cy="6" r="5" stroke="#D3D3D3" stroke-width="2" />
        <rect x="104.167" y="5.5" width="78.6667" height="1" stroke="#D3D3D3" />
        <rect x="183.833" y="5.5" width="1" height="1" stroke="#D3D3D3" />
        <rect x="193.333" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="189.333" cy="6" r="5" stroke="#D3D3D3" stroke-width="2" />
        <rect x="195.833" y="5.5" width="78.6667" height="1" stroke="#D3D3D3" />
        <rect x="275.5" y="5.5" width="1" height="1" stroke="#D3D3D3" />
        <circle cx="281" cy="6" r="5" stroke="#D3D3D3" stroke-width="2" />
      </svg>
    );
  }

  if (step === 2) {
    return (
      <svg width="100%" viewBox="0 0 287 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="10" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle
          cx="6"
          cy="6"
          r="5"
          fill={thumbColorTemp}
          stroke={thumbColorTemp}
          stroke-width="2"
        />
        <path d="M3 6L5 8L9 4" stroke="white" stroke-width="1.5" />
        <rect x="12.5" y="5.5" width="78.6667" height="1" stroke={progressBarColorTemp} />
        <rect x="92.1665" y="5.5" width="1" height="1" stroke={progressBarColorTemp} />
        <rect x="101.667" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="97.6665" cy="6" r="5" stroke={thumbColorTemp} stroke-width="2" />
        <rect x="104.167" y="5.5" width="78.6667" height="1" stroke="#D3D3D3" />
        <rect x="183.833" y="5.5" width="1" height="1" stroke="#D3D3D3" />
        <rect x="193.333" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="189.333" cy="6" r="5" stroke="#D3D3D3" stroke-width="2" />
        <rect x="195.833" y="5.5" width="78.6667" height="1" stroke="#D3D3D3" />
        <rect x="275.5" y="5.5" width="1" height="1" stroke="#D3D3D3" />
        <circle cx="281" cy="6" r="5" stroke="#D3D3D3" stroke-width="2" />
      </svg>
    );
  }
  if (step === 3) {
    return (
      <svg width="100%" viewBox="0 0 287 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="10" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle
          cx="6"
          cy="6"
          r="5"
          fill={thumbColorTemp}
          stroke={thumbColorTemp}
          stroke-width="2"
        />
        <path d="M3 6L5 8L9 4" stroke="white" stroke-width="1.5" />
        <rect x="12.5" y="5.5" width="78.6667" height="1" stroke={progressBarColorTemp} />
        <rect x="102.167" y="5.5" width="1" height="1" stroke={progressBarColorTemp} />
        <circle
          cx="97.6665"
          cy="6"
          r="5"
          fill={thumbColorTemp}
          stroke={thumbColorTemp}
          stroke-width="2"
        />
        <path d="M94.6665 6L96.6665 8L100.667 4" stroke="white" stroke-width="1.5" />
        <rect x="104.167" y="5.5" width="78.6667" height="1" stroke={progressBarColorTemp} />
        <rect x="183.833" y="5.5" width="1" height="1" stroke={progressBarColorTemp} />
        <rect x="193.333" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="189.333" cy="6" r="5" stroke={thumbColorTemp} stroke-width="2" />
        <rect x="195.833" y="5.5" width="78.6667" height="1" stroke="#D3D3D3" />
        <rect x="275.5" y="5.5" width="1" height="1" stroke="#D3D3D3" />
        <circle cx="281" cy="6" r="5" stroke="#D3D3D3" stroke-width="2" />
      </svg>
    );
  }

  if (step === 4) {
    return (
      <svg width="100%" viewBox="0 0 287 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="10" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle
          cx="6"
          cy="6"
          r="5"
          fill={thumbColorTemp}
          stroke={thumbColorTemp}
          stroke-width="2"
        />
        <path d="M3 6L5 8L9 4" stroke="white" stroke-width="1.5" />
        <rect x="12.5" y="5.5" width="78.6667" height="1" stroke={progressBarColorTemp} />
        <rect x="102.167" y="5.5" width="1" height="1" stroke={progressBarColorTemp} />
        <circle
          cx="97.6665"
          cy="6"
          r="5"
          fill={thumbColorTemp}
          stroke={thumbColorTemp}
          stroke-width="2"
        />
        <path d="M94.6665 6L96.6665 8L100.667 4" stroke="white" stroke-width="1.5" />
        <rect x="104.167" y="5.5" width="78.6667" height="1" stroke={progressBarColorTemp} />
        <rect x="193.833" y="5.5" width="1" height="1" stroke={progressBarColorTemp} />
        <circle
          cx="189.333"
          cy="6"
          r="5"
          fill={thumbColorTemp}
          stroke={thumbColorTemp}
          stroke-width="2"
        />
        <path d="M186.333 6L188.333 8L192.333 4" stroke="white" stroke-width="1.5" />
        <rect x="195.833" y="5.5" width="78.6667" height="1" stroke={progressBarColorTemp} />
        <rect x="275.5" y="5.5" width="1" height="1" stroke={progressBarColorTemp} />
        <rect x="285" y="5" width="2" height="2" fill="#D9D9D9" />
        <circle cx="281" cy="6" r="5" stroke={thumbColorTemp} stroke-width="2" />
      </svg>
    );
  }

  return null;
};

export default StepProcess;
