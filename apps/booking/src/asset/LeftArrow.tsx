const LeftArrow = ({ fill }: { fill: string }) => (
  <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0_726_19)">
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M6.80567 1.10815C7.06477 1.36725 7.06477 1.78735 6.80567 2.04644L2.85174 6.00037L6.80567 9.95431C7.06477 10.2134 7.06477 10.6335 6.80567 10.8926C6.54658 11.1517 6.1265 11.1517 5.86741 10.8926L0.975185 6.00037L5.8674 1.10815C6.1265 0.849052 6.54658 0.849052 6.80567 1.10815Z"
        fill={fill}
      />
    </g>
    <defs>
      <clipPath id="clip0_726_19">
        <rect width="7" height="12" fill="white" />
      </clipPath>
    </defs>
  </svg>
);

export default LeftArrow;
