const CloseIcon = (props: { color?: string }) => (
  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M1 1L17 17"
      stroke={props.color ?? '#798183'}
      stroke-width="1.5"
      stroke-linecap="round"
    />
    <path
      d="M1 17L17 1"
      stroke={props.color ?? '#798183'}
      stroke-width="1.5"
      stroke-linecap="round"
    />
  </svg>
);

export default CloseIcon;
