export const convertToFormData = (data: Record<string, string | Blob | undefined>): FormData => {
  const formData = new FormData();
  // eslint-disable-next-line no-restricted-syntax, guard-for-in
  for (const key in data) {
    const value = data[key];
    if (Array.isArray(value)) {
      value.forEach((val) => {
        formData.append(key, val);
      });
    } else if (value) {
      formData.append(key, value);
    }
  }
  return formData;
};
