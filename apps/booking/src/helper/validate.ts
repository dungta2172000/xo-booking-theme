import { t } from '@xo/utils2/src/translate';
import { IFormELementData } from '../../types/data/form';
import { replaceAllString } from './string';

export const validate = (element: IFormELementData, formValue: Record<string, any>) => {
  //Missing required field
  if (
    element.validate.required &&
    (!formValue[element.id] ||
      (Array.isArray(formValue[element.id]) &&
        (!formValue[element.id][0] || !formValue[element.id].length)) ||
      formValue[element.id] === '')
  ) {
    return t(element.validate.requiredText) || 'Missing required information';
    // setFormError({ ...formError, [item.id]: t(item.validate.requiredText) ?? '' });
  }

  //Validate email pattern
  if (element.type === 'email') {
    if (
      formValue[element.id] &&
      formValue[element.id] !== '' &&
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(formValue[element.id])
    ) {
      return 'You have entered an invalid email address!';
    }
  }

  //validate file size
  if (element.type === 'file') {
    const maxSize = (element.validate?.maxSize ?? 1) * 1024 * 1024; // size in MB
    if (formValue[element.id].size > maxSize) {
      return 'Please upload smaller file';
    }
  }

  //validate some field already filled but not pass specific conditions
  if (formValue[element.id]) {
    if (element.type === 'text') {
      const textRangeError =
        (element.validate.minLength && formValue[element.id].length < element.validate.minLength) ||
        (element.validate.maxLength && formValue[element.id].length > element.validate.maxLength);

      if (textRangeError) {
        const lengthError = replaceAllString(t(element.validate.outOfRangeText), {
          minLength: element.validate.minLength,
          maxLength: element.validate.maxLength,
        });
        return lengthError || 'Out of text range';
      }
    }

    //validate number range
    if (element.type === 'number') {
      const numberRangeError =
        (element.validate.min && +formValue[element.id] < element.validate.min) ||
        (element.validate.max && +formValue[element.id] > element.validate.max);

      if (numberRangeError) {
        return t(element.validate.outOfRangeText) || 'Out of number range';
      }
    }
  }

  if (element.validate.pattern && !formValue.match(element.validate.pattern)) {
    return 'Pattern does not match';
  }

  // Pass all condition
  return null;
};
