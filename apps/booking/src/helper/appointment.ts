/* eslint-disable no-restricted-syntax */
import { WEEK_DAYS_SHORT } from '@constants/index';
import dayjs, { Dayjs } from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import isBetween from 'dayjs/plugin/isBetween';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';
import { IOrderedTime, ITimeOffSchedule, IWorkTimeRange, TWeekDay } from 'types/data/general';
import { IService, TPrepareTimeSetting } from 'types/data/service';
import { getHourMinute } from './time';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(customParseFormat);
dayjs.extend(isSameOrAfter);
dayjs.extend(isSameOrBefore);
dayjs.extend(isBetween);

type TSlotTimeReturn = {
  availableTime: Dayjs[];
  totalServiceTime: Dayjs[];
};

export const divideTimeSlot = (
  selectedDate: Dayjs | null,
  adminTimezone: string,
  schedule: IWorkTimeRange[],
  disableWeekDays: TWeekDay[],
  duration: number,
  prepareTime: TPrepareTimeSetting,
): TSlotTimeReturn => {
  const { post, pre } = prepareTime;
  const timeReturn: TSlotTimeReturn = {
    availableTime: [],
    totalServiceTime: [],
  };

  if (selectedDate && !disableWeekDays.includes(selectedDate.format('ddd') as TWeekDay)) {
    const localTimeZone = dayjs.tz.guess();
    // find the start point and end point at admin time zone
    //Example :
    // selectedDate(client timezone) = 0h00p 2021-09-29-> 23h59p 2021-09-30 WED
    // time range in admin timezone can be = 20h00p 2021-09-28 -> 23h59p 2021-09-29 TUE
    //                                 0h00p 2021-09-29 -> 19h59p 2021-09-29 WED
    // then we compare the range in admin timezone with the schedule of admin
    const startDayInAdminTime = selectedDate.tz(adminTimezone);
    const startWeekDayInAdmin = startDayInAdminTime.format('ddd') as TWeekDay;
    const endDayInAdminTime = selectedDate.add(24, 'hour').tz(adminTimezone);
    const endWeekDayInAdmin = endDayInAdminTime.format('ddd') as TWeekDay;

    // find all range in admin that can be fit in admin schedule
    const rangeTemp: { start: Dayjs; end: Dayjs }[] = [];

    const weekDayAdminCanBeFit =
      // end lot day can be 24h00p => still one weekday with start date
      startDayInAdminTime.isSame(endDayInAdminTime.subtract(1, 'second'), 'date')
        ? [startWeekDayInAdmin]
        : [startWeekDayInAdmin, endWeekDayInAdmin];

    for (let i = 0; i < weekDayAdminCanBeFit.length; i++) {
      const weekDay = weekDayAdminCanBeFit[i];
      const weekDaySchedule = schedule.find((scheduleTemp) => scheduleTemp.weekDay === weekDay);

      if (weekDaySchedule) {
        for (const range of weekDaySchedule.timeRanges) {
          const { hour: startHour, minute: startMinute } = getHourMinute(range.start);
          const { hour: endHour, minute: endMinute } = getHourMinute(range.end);

          const dayCurrentIn = (i === 0 ? startDayInAdminTime : endDayInAdminTime).format(
            'DD-MM-YYYY',
          );

          const rangeStartInDayObj = dayjs.tz(
            `${dayCurrentIn} ${startHour}:${startMinute}`,
            'DD-MM-YYYY HH:mm',
            adminTimezone,
          );
          const rangeEndInDayObj = dayjs.tz(
            `${dayCurrentIn} ${endHour}:${endMinute}`,
            'DD-MM-YYYY HH:mm',
            adminTimezone,
          );

          if (
            rangeEndInDayObj.isBetween(startDayInAdminTime, endDayInAdminTime) ||
            rangeStartInDayObj.isBetween(startDayInAdminTime, endDayInAdminTime)
          ) {
            const rangeResult = {
              start: rangeStartInDayObj.tz(localTimeZone),
              end: rangeEndInDayObj.tz(localTimeZone),
            };
            if (rangeStartInDayObj.isBefore(startDayInAdminTime)) {
              rangeResult.start = startDayInAdminTime.tz(localTimeZone);
            }
            if (rangeEndInDayObj.isAfter(endDayInAdminTime)) {
              rangeResult.end = endDayInAdminTime.tz(localTimeZone);
            }

            rangeTemp.push(rangeResult);
          }
        }
      }
    }

    // get all slot fit the admin range found before
    const lastSlotOfDay = selectedDate.add(1, 'day').subtract(duration + post, 'minute');

    for (const range of rangeTemp) {
      const lastSlotOfRange = range.end.subtract(duration, 'minute').subtract(post, 'minute');

      let startSlot = range.start.add(pre, 'minute');

      while (
        startSlot.isSameOrBefore(lastSlotOfDay, 'minute') &&
        startSlot.isSameOrBefore(lastSlotOfRange, 'minute')
      ) {
        timeReturn.totalServiceTime.push(startSlot);
        startSlot = startSlot.add(duration + post, 'minute');
      }
    }

    if (selectedDate.isSame(dayjs(), 'date')) {
      // available time can only be after current moment
      timeReturn.availableTime = timeReturn.totalServiceTime.filter((slot) =>
        slot.isSameOrAfter(dayjs()),
      );
    } else {
      timeReturn.availableTime = timeReturn.totalServiceTime;
    }
  }

  return timeReturn;
};

export const checkExpertWorkTime = (weekDay: TWeekDay, expertWorkTime: IWorkTimeRange[]) => {
  let isDisableDay = true;
  expertWorkTime.forEach((item) => {
    if (item.weekDay === weekDay) {
      isDisableDay = false;
    }
  });

  return isDisableDay;
};

export const getDisableServiceDay = (
  service: IService,
  expertWorkTime: IWorkTimeRange[],
  adminTimezone: string,
) => {
  const workDays: string[] = [];
  if (service && expertWorkTime) {
    // pick ra 7 bừa 7 ngày để check timezone
    for (let i = 0; i < 7; i++) {
      const dayInWeek = dayjs().day(i);
      const dateFormat = dayInWeek.format('DD-MM-YYYY');
      const weekDay = dayInWeek.format('ddd');
      const clientTimezone = dayjs.tz.guess();

      // find this day in schedule
      const weekSchedule = service.workTime.isHasDetail ? service.workTime.detail : expertWorkTime;
      const currentWeekDaySchedule = weekSchedule.find((day) => day.weekDay === weekDay);

      if (currentWeekDaySchedule && currentWeekDaySchedule.timeRanges.length) {
        const { timeRanges } = currentWeekDaySchedule;

        const { hour: hourStart, minute: minuteStart } = getHourMinute(timeRanges[0].start);
        const { hour: hourEnd, minute: minuteEnd } = getHourMinute(
          timeRanges[timeRanges.length - 1].end,
        );

        const dateStartInAdminTimeZone = dayjs.tz(
          `${dateFormat} ${hourStart}:${minuteStart}`,
          'DD-MM-YYYY HH:mm',
          adminTimezone,
        );
        const dateEndInAdminTimeZone = dayjs.tz(
          `${dateFormat} ${hourEnd}:${minuteEnd}`,
          'DD-MM-YYYY HH:mm',
          adminTimezone,
        );

        const dateStartWeekDay = dateStartInAdminTimeZone.tz(clientTimezone).format('ddd');
        workDays.push(dateStartWeekDay);
        const dateEndWeekDay = dateEndInAdminTimeZone.tz(clientTimezone).format('ddd');
        workDays.push(dateEndWeekDay);
      }
    }
  }
  const disableDay = WEEK_DAYS_SHORT.filter((day: string) => !workDays.includes(day));
  return disableDay;
};

export const checkTimeOff = (
  currentListTimeSlot: Dayjs[],
  timeOffSchedule: ITimeOffSchedule[],
  duration: number,
) => {
  timeOffSchedule.forEach((timeOff) => {
    timeOff.detail.forEach(({ start, end }) => {
      const timeOffStart = dayjs(start);
      const timeOffEnd = dayjs(end);

      currentListTimeSlot = currentListTimeSlot.filter((slotStart) => {
        const slotEnd = slotStart.add(duration, 'minute');

        //time off       <--------->
        //slot        <------>
        //            <-------------->
        const isTimeOffStartWrapBySlot = timeOffStart.isBetween(
          slotStart,
          slotEnd,
          'minutes',
          '[)',
        );

        //time off      <--------->
        //slot              <---------->
        //                  <----->
        const isSlotStartInsideTimeOff = slotStart.isBetween(
          timeOffStart,
          timeOffEnd,
          'minutes',
          '[)',
        );
        return !(isTimeOffStartWrapBySlot || isSlotStartInsideTimeOff);
      });
    });
  });

  return currentListTimeSlot;
};

export const checkDuplicateAppointment = (
  totalTimeSlot: Dayjs[],
  currentListTimeSlot: Dayjs[],
  serviceOrdered: IOrderedTime[],
  duration: number,
  prepareTime?: TPrepareTimeSetting,
) => {
  serviceOrdered.forEach((order) => {
    let newListTimeSlot: Dayjs[] = [];
    let newTotalTime: Dayjs[] = [];
    const { start, end } = order.date;

    const orderStart = dayjs(start).subtract(prepareTime?.pre || 0, 'minute');
    const orderEnd = dayjs(end).add(prepareTime?.post || 0, 'minute');

    // map for slot temp
    newListTimeSlot = currentListTimeSlot.map((slot) => slot);
    newTotalTime = totalTimeSlot.map((slot) => slot);

    //checking duplicate appointment
    newListTimeSlot = newListTimeSlot.filter((slotStart) => {
      const slotEnd = slotStart.add(duration, 'minute');
      //order         <--------->
      //slot        <------>
      //            <-------------->
      const isOrderStartWrapBySlot = orderStart.isBetween(slotStart, slotEnd, 'minutes', '[)');

      //order         <--------->
      //slot              <---------->
      //                  <----->
      const isSlotStartInsideOrder = slotStart.isBetween(orderStart, orderEnd, 'minutes', '[)');

      return !(isSlotStartInsideOrder || isOrderStartWrapBySlot);
    });

    if (newTotalTime.length) totalTimeSlot = newTotalTime.map((time) => time);
    newTotalTime = [];
    if (newListTimeSlot.length) currentListTimeSlot = newListTimeSlot.map((time) => time);
    newListTimeSlot = [];
  });

  return { totalTimeSlot, availableTimeSlot: currentListTimeSlot };
};
