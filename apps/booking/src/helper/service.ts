import { IService } from 'types/data/service';

export const getServiceInProductPage = (serviceList: [IService]) => {
  const defaultProductId = (document.querySelector('.xo-booking') as any).dataset.id;

  if (defaultProductId && serviceList?.length) {
    const serviceDetail = serviceList.find(
      (serviceTemp) => serviceTemp.product.id === +defaultProductId,
    );

    return serviceDetail;
  } else {
    return null;
  }
};
