import { t } from '@xo/utils2/src/translate';
import { TTimeUnit } from 'types/data/settings';

export const replaceAllString = (string: string, replaceString: Record<string, any>) => {
  if (!string) {
    return string;
  }
  let newString = string;
  // eslint-disable-next-line no-restricted-syntax, guard-for-in
  for (const x in replaceString) {
    newString = newString.replace(new RegExp(`{${x}}`, 'g'), replaceString[x]);
  }
  return newString;
};

export const getServiceTimeUnit = (time: number, timeUnit: TTimeUnit) =>
  t(time === 1 ? timeUnit.singular : timeUnit.plural);
