export const checkCartItems = async (variantId: number, timeStart: Date) => {
  try {
    const cartResponse = await fetch('/cart.js');
    const data = await cartResponse.json();
    const isCartItemDuplicated = data.items.some(
      (item: any) => item.id === variantId && item.properties.timeStart === timeStart.toISOString(),
    );
    return isCartItemDuplicated;
  } catch (error) {
    console.log(error);
    return false;
  }
};
