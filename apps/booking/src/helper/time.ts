export const getHourMinute = (time: number) => {
  const minute = time % 100;
  const hour = (time - minute) / 100;

  return { minute, hour };
};
