import { IWorkTimeRange } from './general';

export type TServiceSetting = {
  multiVariantOrder: boolean;
  skipCheckout: boolean;
};

export type TPrepareTimeSetting = {
  post: number;
  pre: number;
};

export interface IService {
  _id: string;
  product: IProduct;
  workTime: {
    detail: IWorkTimeRange[];
    isHasDetail: boolean;
  };
  isCustomProduct: boolean;
  title: string;
  description: string;
  duration: number;
  media: any[];
  settings: TServiceSetting;
  prepareTime: TPrepareTimeSetting;
  customFormId: string;
}

export interface IProduct {
  image: IImage;
  id: number;
  title: string;
  description: string;
  handle: string;
  variants: IVariant[];
}

export interface IImage {
  id: string;
  src: string;
  alt: string;
  mediaContentType: string;
}

export interface IVariant {
  title: string;
  price: string;
  id: number;
}
