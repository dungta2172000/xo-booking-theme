import { ICustomer } from './customer';
import { IService } from './service';

export interface ICustomAttributeSubmit {
  timeStart: Date | null;
  timeEnd: Date | null;
  customerTimezone: string | null;
  dataFormId: string | null;
  form: Record<string, any>;
  service: IService | null;
  customer: ICustomer;
  variantId: number | null;
}
