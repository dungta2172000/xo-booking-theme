export type TWeekDay = 'Sun' | 'Mon' | 'Tue' | 'Wed' | 'Thu' | 'Fri' | 'Sat';

export interface IWorkTime {
  start: Date | string;
  end: Date | string;
}

export interface IOrderedTime {
  date: IWorkTime & {
    customerTimezone: string;
  };
}

export interface IWorkTimeRange {
  weekDay: string;
  timeRanges: [
    {
      start: number;
      end: number;
    },
  ];
}

export interface ITimeOffSchedule {
  date: Date;
  type: 'full' | 'range';
  detail: IWorkTime[];
}

export interface IExpertWorkTime {
  expertWorkTime: IWorkTimeRange[];
}

export type TResRequest = {
  data: any;
  error: string | null;
};

export enum TToastType {
  success,
  failed,
}

export interface IToast {
  show: boolean;
  position: 'bottom-left' | 'bottom-right';
  title: string;
  description: string;
  type?: TToastType;
  onClose?: () => void;
}
