export interface IElementOptions {
  value: any;
  label: {
    [key: string]: string;
  };
  id: string;
}

export interface IFormELementData {
  options?: IElementOptions[];
  allowOtherValue?: boolean;
  multiple?: boolean;
  type: string;
  label: Record<string, string>;
  hideLabel: boolean;
  hideField: boolean;
  id: string;
  defaultValue: string;
  placeholder?: Record<string, string>;
  helpText: Record<string, string>;
  className: string;
  name: string;
  cols?: number;
  rows?: number;
  validate: {
    required: boolean;
    requiredText?: Record<string, string>;
    minLength?: number;
    maxLength?: number;
    min?: number;
    max?: number;
    outOfRangeText?: Record<string, string>;
    pattern?: string;
    message?: Record<string, string>;
    maxSize?: number;
    fileType?: string;
  };
  otherValue?: {
    allowOtherValue: boolean;
    value: Record<string, any>;
  };
  allowMultiple?: boolean;
}

export interface IFormElementProps {
  element: IFormELementData;
  value: any;
  hasError?: boolean;
  error?: string;
  onBlur?: any;
  defaultValue?: any;
  onChange: (value: any) => void;
}

export interface IFormSettings {
  allowRecapcha: {
    secretKey: string;
    siteKey: string;
    allowRecapchaVerify: boolean;
  };
  submitButtonText: {
    [key: string]: string;
  };
  successMessage: {
    [key: string]: string;
  };
  errorMessage: {
    [key: string]: string;
  };
  allowMultipleStep: {
    allowMultipleStep: boolean;
    nextText: {
      [key: string]: string;
    };
    previousText: {
      [key: string]: string;
    };
  };
  hideTitleAndDescription: boolean;
  flowType: {
    flow: string;
    popupDisplayText: {
      [key: string]: string;
    };
    popupDisplayTitle: {
      [key: string]: string;
    };
  };
  submitSuccessBehavior: {
    behavior: string;
    redirectUrl: string;
  };
}

export interface IForm {
  __typename: string;
  _id: string;
  id: number;
  title: string;
  description: string;
  createdAt: string;
  displayTitleType: string;
  data: {
    fields: IFormELementData[];
    settings: {
      form: IFormSettings;
      email: {
        allowSendMail: boolean;
        subject: string;
        toEmail: string;
        content: string;
      };
    };
    conditions: any[];
  };
}
