import { TMultiLanguage } from '@xo/utils2/src/translate';

export type TWeekDayLowerCase = 'mon' | 'tue' | 'wed' | 'thu' | 'fri' | 'sat' | 'sun';
export type TMonth =
  | 'jan'
  | 'feb'
  | 'mar'
  | 'apr'
  | 'may'
  | 'jun'
  | 'jul'
  | 'aug'
  | 'sep'
  | 'oct'
  | 'nov'
  | 'dec';

export type TStepTitle = {
  title: TMultiLanguage;
  subTitle: TMultiLanguage;
};
export type TToastNotiMultiLang = {
  title: TMultiLanguage;
  description: TMultiLanguage;
};

export type TTimeUnit = {
  singular: TMultiLanguage;
  plural: TMultiLanguage;
};

export type TLabelNaming = {
  skipCheckoutForm: {
    firstName: TMultiLanguage;
    lastName: TMultiLanguage;
    email: TMultiLanguage;
    emailRequiredText: TMultiLanguage;
    emailHelperText: TMultiLanguage;
  };
  progress: {
    step1: TStepTitle;
    step2: TStepTitle;
    step3: TStepTitle;
    step4: TStepTitle;
  };
  confirmInfo: {
    schedule: TMultiLanguage;
    changeButton: TMultiLanguage;
    customer: TMultiLanguage;
  };
  button: {
    back: TMultiLanguage;
    next: TMultiLanguage;
    confirm: TMultiLanguage;
  };
  dayNotAvailable: TMultiLanguage;
  calendar: {
    time: TTimeUnit;
    weekday: Record<TWeekDayLowerCase, TMultiLanguage>;
    month: Record<TMonth, TMultiLanguage>;
  };
  toastNotification: {
    bookingSuccess: TToastNotiMultiLang;
    bookingFail: TToastNotiMultiLang;
  };
};

export type TCustomAppearanceLayout1 = {
  variant: number;
  cardPerRow: number;
};

export type TCustomAppearanceLayout2 = {
  variant: number;
};

export type TAppearance = TCustomAppearanceLayout1 & TCustomAppearanceLayout2;

export type TStoreLayout = {
  layoutType: number;
  appearance: TAppearance;
  colorTheme: string;
};

export type TTimeSetting = {
  timezone: string;
  hourFormat: string;
};

export type TBookingRuleSetting = {
  bookBefore: number;
  allowDuplicateAppointment: boolean;
};

export interface ISettings {
  bookingRule: TBookingRuleSetting;
  timeFormat: TTimeSetting;
  storeLayout: TStoreLayout;
  translation: TLabelNaming;
}
