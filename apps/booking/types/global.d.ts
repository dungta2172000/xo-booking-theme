/// <reference path="../../../types/global.d.ts" />
import * as preact from 'preact';

type customElementsClassName = { class?: string };

interface customElementsClassName extends preact.JSX.HTMLAttributes<HTMLElement> {
  class?: string;
}

declare module 'preact' {
  namespace JSX {
      interface IntrinsicElements {
          'xo-modal': customElementsClassName;
          'xo-modal-trigger': customElementsClassName;
          'xo-modal-pan': customElementsClassName;
          'xo-carousel': customElementsClassName;
          'xo-carousel-inner': customElementsClassName;
          'xo-carousel-list': customElementsClassName;
          'xo-carousel-slide': customElementsClassName;
          'xo-carousel-thumbnail': customElementsClassName;
          'xo-carousel-prev': customElementsClassName;
          'xo-carousel-next': customElementsClassName;
          'xo-carousel-pagination': customElementsClassName;
      }
  }
}
