module.exports = {
  root: true,
  extends: ['preact', 'airbnb-typescript'],
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },

  rules: {
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'object-curly-newline': 'off',
    'no-underscore-dangle': ['error', { allow: ['_id'] }],
    'operator-linebreak': ['error', 'after', { overrides: { '?': 'before', ':': 'before' } }],
    // '@typescript-eslint/naming-convention': ['error', { custom: { regex: '_id', match: true } }],
  },
};
