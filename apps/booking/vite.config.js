import { defineConfig } from 'vite';
import preact from '@preact/preset-vite';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [preact(), tsconfigPaths()],
  build: {
    rollupOptions: {
      output: {
        entryFileNames: 'xo-booking.js',
        assetFileNames(info) {
          const { name } = info;
          if (name.endsWith('.css')) return 'xo-booking.css';
          return '[name].[ext]';
        },
        manualChunks: {},
      },
    },
    outDir: './dist',
    // outDir: '../shopify/theme/assets',
  },
  css: {
    modules: {
      localsConvention: 'camelCase',
    },
  },
  base: './',
});
