# Turborepo

## Install pnpm

https://pnpm.io/installation

## Commands


```
pnpm i
pnpm add --save-dev @xo/types@workspace --filter faqs
```

Update turbo:
```
npx @turbo/codemod migrate
```

## Remove node_modules folders

```
find . -name 'node_modules' -type d -prune -print -exec rm -rf '{}' \;
```

Các folder có node_modules (trường hợp remove tay):

```
./node_modules
./packages/ui/node_modules
./packages/utils/node_modules
./packages/eslint-config-preact/node_modules
./packages/eslint-config-custom/node_modules
./packages/xo-form/node_modules
./apps/booking/node_modules
./apps/lethnic/node_modules
./apps/docs/node_modules
./apps/faqs/node_modules
./apps/sizecharts/node_modules
```
